package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.NewsAuthorDAO;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class NewsAuthorDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsAuthorDAOTest {

    /**
     * The news author dao.
     */
    @Autowired
    private NewsAuthorDAO newsAuthorDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test news author creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/newsAuthorCreationData.xml")
    @ExpectedDatabase(value = "classpath:data/expected/newsAuthorCreationData.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testNewsAuthorCreation() throws DAOException {
        newsAuthorDAO.create(0L, 0L);
    }

    /**
     * Test find author ids by news id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadAuthorIdsByNewsId() throws DAOException {
        int sSize = newsAuthorDAO.findAuthorIdsByNewsId(0L).size();

        assertEquals(1, sSize);
    }

    /**
     * Test find news ids by author id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadNewsIdsByAuthorId() throws DAOException {
        int sSize = newsAuthorDAO.readNewsIdsByAuthorId(6L).size();

        assertEquals(6, sSize);
    }

    /**
     * Test author delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/expected/deleteAuthorData.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAuthorDelete() throws DAOException {
        newsAuthorDAO.delete(1L, 1L);
    }
}


