package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.AuthorDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Date;

/**
 * The Class AuthorDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class,})
public class AuthorDAOTest {

    /**
     * The author dao.
     */
    @Autowired
    private AuthorDAO authorDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test author creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/data.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAuthorCreation() throws DAOException {
        Author author = new Author("John Smith", new Timestamp(new Date().getTime()));
        author = authorDAO.create(author);

        assertEquals("John Smith", author.getName());
    }

    /**
     * Test find author number3.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadAuthorNumber3() throws DAOException {
        Author author = authorDAO.find(3L);
        assertEquals("Jarred Snow", author.getName());
    }

    /**
     * Test author delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testAuthorDelete() throws DAOException {
        Author author = authorDAO.find(9L);
        assertNotNull(author);

        authorDAO.delete(9L);

        author = authorDAO.find(9L);
        assertNull(author);
    }

    /**
     * Test author update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testAuthorUpdate() throws DAOException {
        Author author = authorDAO.find(0L);
        author.setName("updated");
        Timestamp ts = new Timestamp(new Date().getTime());
        author.setExpired(ts);

        authorDAO.update(author);

        author = authorDAO.find(0L);
        assertEquals("updated", author.getName());
        assertEquals(ts, author.getExpired());
    }
}