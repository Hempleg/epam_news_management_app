package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.NewsTagDAO;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class NewsTagDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsTagDAOTest {

    /**
     * The news tag dao.
     */
    @Autowired
    private NewsTagDAO newsTagDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test news tag creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/newsTagCreationData.xml")
    @ExpectedDatabase(value = "classpath:data/expected/newsTagCreationData.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testNewsTagCreation() throws DAOException {
        newsTagDAO.create(0L, 9L);
        newsTagDAO.create(0L, 8L);
        newsTagDAO.create(0L, 7L);
        newsTagDAO.create(0L, 6L);
        newsTagDAO.create(0L, 5L);
    }

    /**
     * Test find tag ids by news id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadTagIdsByNewsId() throws DAOException {
        int sSize = newsTagDAO.findTagIdsByNewsId(0L).size();

        assertEquals(8, sSize);
    }

    /**
     * Test find news ids by tag id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadNewsIdsByTagId() throws DAOException {
        int sSize = newsTagDAO.readNewsIdsByTagId(3L).size();

        assertEquals(2, sSize);
    }

    /**
     * Test tag delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/expected/newsTagDeleteData.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testTagDelete() throws DAOException {
        newsTagDAO.delete(0L, 3L);
    }
}


