package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.UserDAO;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class UserDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class UserDAOTest {

    /**
     * The user dao.
     */
    @Autowired
    private UserDAO userDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test user creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/data.xml", type = DatabaseOperation.DELETE_ALL)
    public void testUserCreation() throws DAOException {
        User user = new User("NewUser", "NewLogin", "Password", null);
        user = userDAO.create(user);

        user = userDAO.find(user.getId());

        assertEquals("NewUser", user.getUserName());
    }

    /**
     * Test find user number10.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadUserNumber10() throws DAOException {
        User user = userDAO.find(10L);
        assertEquals("Slava", user.getUserName());
    }

    /**
     * Test user delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testUserDelete() throws DAOException {
        User user = userDAO.find(19L);
        assertNotNull(user);

        userDAO.delete(19L);

        user = userDAO.find(19L);
        assertNull(user);
    }

    /**
     * Test user update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testUserUpdate() throws DAOException {
        User user = userDAO.find(0L);
        user.setUserName("updated");
        user.setLogin("updated");
        user.setPassword("updated");

        userDAO.update(user);

        user = userDAO.find(0L);
        assertEquals("updated", user.getLogin());
        assertEquals("updated", user.getUserName());
        assertEquals("updated", user.getPassword());
    }
}


