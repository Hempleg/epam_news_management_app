package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.UserRoleDAO;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class UserRoleDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class UserRoleDAOTest {

    /**
     * The user role dao.
     */
    @Autowired
    private UserRoleDAO userRoleDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test user role creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/userRoleSpecData.xml")
    public void testUserRoleCreation() throws DAOException {
        userRoleDAO.create(0L, 0L);

        Long roleId = userRoleDAO.readRoleIdByUserId(0L);

        assertEquals(new Long(0), roleId);
    }

    /**
     * Test find user role id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadUserRoleId() throws DAOException {
        Long roleId = userRoleDAO.readRoleIdByUserId(7L);

        assertEquals(new Long(2), roleId);
    }

    /**
     * Test find user ids by role id.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadUserIdsByRoleId() throws DAOException {
        int sSize = userRoleDAO.readUserIdsByRoleId(3L).size();

        assertEquals(4, sSize);
    }

    /**
     * Test role delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testRoleDelete() throws DAOException {
        Long roleId = userRoleDAO.readRoleIdByUserId(3L);

        assertNotNull(roleId);

        userRoleDAO.delete(3L, 3L);
        roleId = userRoleDAO.readRoleIdByUserId(3L);

        assertNull(roleId);
    }
}


