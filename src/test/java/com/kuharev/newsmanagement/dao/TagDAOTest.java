package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.TagDAO;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class TagDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TagDAOTest {

    /**
     * The tag dao.
     */
    @Autowired
    private TagDAO tagDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test tag creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/data.xml", type = DatabaseOperation.DELETE_ALL)
    public void testTagCreation() throws DAOException {
        Tag tag = new Tag("NewTag");
        tag = tagDAO.create(tag);

        tag = tagDAO.find(tag.getId());

        assertEquals("NewTag", tag.getName());
    }

    /**
     * Test find tag number10.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadTagNumber10() throws DAOException {
        Tag tag = tagDAO.find(0L);
        assertEquals("tag1", tag.getName());
    }

    /**
     * Test tag delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testTagDelete() throws DAOException {
        Tag tag = tagDAO.find(9L);
        assertNotNull(tag);

        tagDAO.delete(9L);

        tag = tagDAO.find(9L);
        assertNull(tag);
    }

    /**
     * Test tag update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testTagUpdate() throws DAOException {
        Tag tag = tagDAO.find(0L);
        tag.setName("updated");

        tagDAO.update(tag);

        tag = tagDAO.find(0L);
        assertEquals("updated", tag.getName());
    }
}


