package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.NewsDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * The Class NewsDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsDAOTest {

    /**
     * The news dao.
     */
    @Autowired
    private NewsDAO newsDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test news creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/data.xml", type = DatabaseOperation.DELETE_ALL)
    public void testNewsCreation() throws DAOException {
        News news = new News("NewTitle", "NewShortText", "NewFullText",
                new Timestamp(new Date().getTime()),
                new Timestamp(new Date().getTime()), null, null, null);
        news = newsDAO.create(news);

        news = newsDAO.find(news.getId());

        assertEquals("NewTitle", news.getTitle());
    }

    /**
     * Test find news number0.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadNewsNumber0() throws DAOException {
        News news = newsDAO.find(0L);
        assertEquals("Title1", news.getTitle());
    }

    /**
     * Test news delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testNewsDelete() throws DAOException {
        News news = newsDAO.find(3L);
        assertNotNull(news);

        newsDAO.delete(3L);

        news = newsDAO.find(3L);
        assertNull(news);
    }

    /**
     * Test news update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testNewsUpdate() throws DAOException {
        News news = newsDAO.find(0L);
        news.setTitle("updated");
        news.setShortText("updated");
        news.setFullText("updated");
        Timestamp ts = new Timestamp(new Date().getTime());
        news.setCreationDate(ts);
        news.setModificationDate(ts);

        newsDAO.update(news);

        news = newsDAO.find(0L);
        assertEquals("updated", news.getTitle());
        assertEquals("updated", news.getFullText());
        assertEquals("updated", news.getShortText());
        assertEquals(ts, news.getModificationDate());
        assertEquals(ts, news.getCreationDate());

    }

    /**
     * Test find by serch criteria.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testFindBySerchCriteria() throws DAOException {
        Tag tag0 = new Tag();
        Tag tag1 = new Tag();
        Tag tag2 = new Tag();
        tag0.setId(0);
        tag1.setId(0);
        tag2.setId(0);

        Author author = new Author();
        author.setId(0);

        SerchCriteria sc = new SerchCriteria(author, Arrays.asList(tag0, tag1, tag2));

        List<News> newsList = newsDAO.findBySerchCriteria(sc);

        assertEquals(1, newsList.size());
        assertEquals("Title1", newsList.get(0).getTitle());

    }
}


