package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.RoleDAO;
import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

/**
 * The Class RoleDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class RoleDAOTest {

    /**
     * The role dao.
     */
    @Autowired
    private RoleDAO roleDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test role creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/data.xml", type = DatabaseOperation.DELETE_ALL)
    public void testRoleCreation() throws DAOException {
        Role role = new Role("NewRole");
        role = roleDAO.create(role);

        role = roleDAO.find(role.getId());
        assertEquals("NewRole", role.getName());
    }

    /**
     * Test find role number0.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadRoleNumber0() throws DAOException {
        Role role = roleDAO.find(0L);
        assertEquals("User", role.getName());
    }

    /**
     * Test role delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testRoleDelete() throws DAOException {
        Role role = roleDAO.find(3L);
        assertNotNull(role);

        roleDAO.delete(3L);

        role = roleDAO.find(3L);
        assertNull(role);
    }

    /**
     * Test role update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testRoleUpdate() throws DAOException {
        Role role = roleDAO.find(0L);
        role.setName("updated");

        roleDAO.update(role);

        role = roleDAO.find(0L);
        assertEquals("updated", role.getName());
    }
}


