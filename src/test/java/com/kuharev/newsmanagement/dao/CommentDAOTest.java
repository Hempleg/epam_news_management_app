package com.kuharev.newsmanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.kuharev.newsmanagement.dao.impl.CommentDAO;
import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.DAOException;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Date;

/**
 * The Class CommentDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class CommentDAOTest {

    /**
     * The comment dao.
     */
    @Autowired
    private CommentDAO commentDAO;

    /**
     * Test check data loaded.
     *
     * @throws Exception the exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    @ExpectedDatabase(value = "classpath:data/data.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCheckDataLoaded() throws Exception {
    }

    /**
     * Test comment with one argument not supported.
     *
     * @throws DAOException the DAO exception
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testCommentWithOneArgumentNotSupported() throws DAOException {
        commentDAO.create(new Comment());
    }

    /**
     * Test comment creation.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup(value = "classpath:data/commentCreationData.xml")
    public void testCommentCreation() throws DAOException {
        Comment comment = new Comment("NewComment",
                new Timestamp(new Date().getTime()));
        comment = commentDAO.create(0L, comment);

        comment = commentDAO.find(comment.getId());

        assertEquals("NewComment", comment.getText());
    }

    /**
     * Test find comment number9.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testReadCommentNumber9() throws DAOException {
        Comment comment = commentDAO.find(9L);
        assertEquals("Comment10", comment.getText());
    }

    /**
     * Test comment delete.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testCommentDelete() throws DAOException {
        Comment comment = commentDAO.find(3L);
        assertNotNull(comment);

        commentDAO.delete(3L);

        comment = commentDAO.find(3L);
        assertNull(comment);
    }

    /**
     * Test comment update.
     *
     * @throws DAOException the DAO exception
     */
    @Test
    @DatabaseSetup("classpath:data/data.xml")
    public void testCommentUpdate() throws DAOException {
        Comment comment = commentDAO.find(0L);
        comment.setText("updated");
        Timestamp ts = new Timestamp(new Date().getTime());
        comment.setCreationDate(ts);

        commentDAO.update(comment);

        comment = commentDAO.find(0L);
        assertEquals("updated", comment.getText());
        assertEquals(ts, comment.getCreationDate());

    }
}


