package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


import java.util.Arrays;
import java.util.List;

import com.kuharev.newsmanagement.dao.impl.RoleDAO;
import com.kuharev.newsmanagement.dao.impl.UserRoleDAO;
import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.RoleServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class RoleServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class RoleServiceTest {

    /**
     * The role data loader.
     */
    @Autowired
    private DataLoader<Role> roleDataLoader;

    /**
     * The role dao.
     */
    @Mock
    private RoleDAO roleDAO;

    /**
     * The user service.
     */
    @Mock
    private UserService userService;

    /**
     * The user role dao.
     */
    @Mock
    private UserRoleDAO userRoleDAO;

    /**
     * The role service.
     */
    @InjectMocks
    private RoleServiceImpl roleService;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Sets the role data loader.
     *
     * @param roleDataLoader the new role data loader
     */
    public void setRoleDataLoader(DataLoader<Role> roleDataLoader) {
        this.roleDataLoader = roleDataLoader;
    }

    /**
     * Test role service find all.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testRoleServiceReadAll() throws DAOException, ServiceException, DataLoaderException {
        List<Role> expected = roleDataLoader.loadData("data/data.xml");
        when(roleDAO.readAll()).thenReturn(expected);

        assertEquals(expected, roleService.getAllRoles());
    }

    /**
     * Test find role.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testFindRole() throws DAOException, ServiceException, DataLoaderException {
        Role role = roleDataLoader.loadData("data/data.xml").get(0);
        when(roleDAO.find(role.getId())).thenReturn(role);

        assertEquals(role, roleService.findRole(role.getId()));
    }

    /**
     * Test user service delete user.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testUserServiceDeleteUser() throws DAOException, ServiceException {
        roleService.deleteRole(anyLong());
    }

    /**
     * Test create role.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testCreateRole() throws DAOException, ServiceException {
        Role role = new Role();
        Role retRole = new Role();

        Long roleId = anyLong();
        retRole.setId(roleId);

        when(roleDAO.create(role)).thenReturn(retRole);

        roleService.addRole(role);

        assertEquals(role.getId(), retRole.getId());
    }

    /**
     * Test change user role.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testChangeUserRole() throws DAOException, ServiceException {
        Long id = anyLong();

        roleService.changeUserRole(id, id);
    }

    /**
     * Test find role by user.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testFindRoleByUser() throws DAOException, ServiceException {
        Role role = new Role("Role");
        Role retRole = null;

        Long roleId = anyLong();
        Long userId = roleId;
        role.setId(roleId);

        when(userRoleDAO.readRoleIdByUserId(userId)).thenReturn(roleId);
        when(roleDAO.find(roleId)).thenReturn(role);

        retRole = roleService.findRoleByUserId(userId);

        assertEquals(role, retRole);
    }

    /**
     * Test find users by role.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testFindUsersByRole() throws DAOException, ServiceException {
        User user = new User();
        Long userId = anyLong();
        user.setId(userId);

        Long roleId = userId;

        when(userRoleDAO.readUserIdsByRoleId(roleId)).thenReturn(Arrays.asList(userId));
        when(userService.findUser(userId)).thenReturn(user);

        List<User> users = roleService.findUsersByRole(roleId);

        assertEquals(Arrays.asList(user), users);
    }
}
