package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyLong;

import java.util.List;

import static org.junit.Assert.*;

import com.kuharev.newsmanagement.dao.impl.AuthorDAO;
import com.kuharev.newsmanagement.dao.impl.NewsAuthorDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.AuthorServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class AuthorServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class AuthorServiceTest {

    /**
     * Author data loader bean.
     */
    @Autowired
    private DataLoader<Author> authorDataLoader;

    /**
     * The author dao.
     */
    @Mock
    private AuthorDAO authorDAO;

    /**
     * The news author dao.
     */
    @Mock
    private NewsAuthorDAO newsAuthorDAO;

    /**
     * The news service.
     */
    @Mock
    private NewsService newsService;

    /**
     * The author service.
     */
    @InjectMocks
    private AuthorServiceImpl authorService;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Sets the author data loader.
     *
     * @param authorDataLoader the new author data loader
     */
    public void setAuthorDataLoader(DataLoader<Author> authorDataLoader) {
        this.authorDataLoader = authorDataLoader;
    }


    /**
     * Test get all authors.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException the data loader exception
     */
    @Test
    public void testGetAllAuthors() throws DAOException, ServiceException, DataLoaderException {
        List<Author> expected = authorDataLoader.loadData("data/data.xml");
        when(authorDAO.findAll()).thenReturn(expected);

        assertEquals(expected, authorService.getAllAuthors());
    }

    /**
     * Test find author.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testFindAuthor() throws DAOException, ServiceException, DataLoaderException {
        Author author = authorDataLoader.loadData("data/data.xml").get(0);
        when(authorDAO.find(author.getId())).thenReturn(author);

        assertEquals(author, authorService.findAuthor(author.getId()));
    }

    /**
     * Test delete author.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testDeleteAuthor() throws DAOException, ServiceException {
        authorService.deleteAuthor(anyLong());
    }

    /**
     * Test create author.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testCreateAuthor() throws DAOException, ServiceException {
        Author author = new Author();
        Author retAuthor = new Author();
        Long authorId = anyLong();
        retAuthor.setId(authorId);

        when(authorDAO.create(author)).thenReturn(retAuthor);

        authorService.addAuthor(author);

        assertEquals(retAuthor.getId(), author.getId());
    }
}
