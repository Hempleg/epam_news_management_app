package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


import java.util.Arrays;
import java.util.List;

import com.kuharev.newsmanagement.dao.impl.NewsDAO;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.NewsServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class NewsServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class NewsServiceTest {


    /**
     * The news data loader.
     */
    @Autowired
    private DataLoader<News> newsDataLoader;

    /**
     * The news dao.
     */
    @Mock
    private NewsDAO newsDAO;

    /**
     * The tag service.
     */
    @Mock
    private TagService tagService;

    /**
     * The author service.
     */
    @Mock
    private AuthorService authorService;

    /**
     * The comment service.
     */
    @Mock
    private CommentService commentService;

    /**
     * The news service.
     */
    @InjectMocks
    private NewsServiceImpl newsService;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Sets the news data loader.
     *
     * @param newsDataLoader the new news data loader
     */
    public void setNewsDataLoader(DataLoader<News> newsDataLoader) {
        this.newsDataLoader = newsDataLoader;
    }

    /**
     * Test news service find all.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testNewsServiceReadAll() throws DAOException, ServiceException, DataLoaderException {
        List<News> expected = newsDataLoader.loadData("data/data.xml");
        when(newsDAO.findAll()).thenReturn(expected);

        assertEquals(expected, newsService.getAllNews());
    }

    /**
     * Test news service find news.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testNewsServiceFindNews() throws DAOException, ServiceException {
        News news = new News();
        Long newsId = anyLong();
        news.setId(newsId);

        when(newsDAO.find(newsId)).thenReturn(news);

        assertEquals(news, newsService.findNews(newsId));
    }

    /**
     * Test news service delete news.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testNewsServiceDeleteNews() throws DAOException, ServiceException {
        newsService.deleteNews(anyLong());
    }

    /**
     * Test news service create news.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testNewsServiceCreateNews() throws DAOException, ServiceException {
        News news = new News();
        News retNews = new News();

        Long newsId = anyLong();
        retNews.setId(newsId);

        when(newsDAO.create(news)).thenReturn(retNews);

        newsService.addNews(news);

        assertEquals(retNews.getId(), news.getId());
    }

    /**
     * Test find news by serch criteria.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testFindNewsBySerchCriteria() throws DAOException, ServiceException {
        News news = new News();
        SerchCriteria sc = new SerchCriteria();

        when(newsDAO.findBySerchCriteria(sc)).thenReturn(Arrays.asList(news));

        List<News> newsList = newsService.findNewsBySerchCriteria(sc);

        assertEquals(Arrays.asList(news), newsList);
    }

}
