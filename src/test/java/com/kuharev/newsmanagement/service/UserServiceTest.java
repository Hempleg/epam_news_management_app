package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


import java.util.List;

import com.kuharev.newsmanagement.dao.impl.UserDAO;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.UserServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class UserServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class UserServiceTest {

    /**
     * User data loader bean.
     */
    @Autowired
    private DataLoader<User> userDataLoader;

    /**
     * The user dao.
     */
    @Mock
    private UserDAO userDAO;

    /**
     * The role service.
     */
    @Mock
    private RoleService roleService;

    /**
     * The user service.
     */
    @InjectMocks
    private UserServiceImpl userService;

    /**
     * Sets the user data loader.
     *
     * @param userDataLoader the new user data loader
     */
    public void setUserDataLoader(DataLoader<User> userDataLoader) {
        this.userDataLoader = userDataLoader;
    }

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test user service find all.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testUserServiceReadAll() throws DAOException, ServiceException, DataLoaderException {
        List<User> expected = userDataLoader.loadData("data/data.xml");
        when(userDAO.readAll()).thenReturn(expected);

        assertEquals(expected, userService.getAllUsers());
    }

    /**
     * Test user service find user.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testUserServiceFindUser() throws DAOException, ServiceException, DataLoaderException {
        User user = userDataLoader.loadData("data/data.xml").get(0);
        when(userDAO.find(user.getId())).thenReturn(user);

        assertEquals(user, userService.findUser(user.getId()));
    }

    /**
     * Test user service delete user. Should not throw exception while running.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testUserServiceDeleteUser() throws DAOException, ServiceException {
        userService.deleteUser(anyLong());
    }

    /**
     * Test user service create user.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testUserServiceCreateUser() throws DAOException, ServiceException {
        User user = new User();
        User retUser = new User();

        Long userId = anyLong();
        retUser.setId(userId);

        when(userDAO.create(user)).thenReturn(retUser);

        userService.addUser(user);

        assertEquals(retUser.getId(), user.getId());
    }
}
