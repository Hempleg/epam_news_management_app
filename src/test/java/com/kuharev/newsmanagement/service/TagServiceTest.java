package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


import java.util.Arrays;
import java.util.List;

import com.kuharev.newsmanagement.dao.impl.NewsTagDAO;
import com.kuharev.newsmanagement.dao.impl.TagDAO;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.TagServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class TagServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class TagServiceTest {

    /**
     * The tag data loader.
     */
    @Autowired
    private DataLoader<Tag> tagDataLoader;

    /**
     * The tag dao.
     */
    @Mock
    private TagDAO tagDAO;

    /**
     * The news tag dao.
     */
    @Mock
    private NewsTagDAO newsTagDAO;

    /**
     * The news service.
     */
    @Mock
    private NewsService newsService;

    /**
     * The tag service.
     */
    @InjectMocks
    private TagServiceImpl tagService;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    public void setTagDataLoader(DataLoader<Tag> tagDataLoader) {
        this.tagDataLoader = tagDataLoader;
    }

    /**
     * Test tag service find all.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException
     */
    @Test
    public void testTagServiceReadAll() throws DAOException, ServiceException, DataLoaderException {
        List<Tag> expected = tagDataLoader.loadData("data/data.xml");
        when(tagDAO.readAll()).thenReturn(expected);

        assertEquals(expected, tagService.getAllTags());
    }

    /**
     * Test tag service find tag.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testTagServiceFindTag() throws DAOException, ServiceException {
        Tag tag = new Tag();
        Long tagId = anyLong();
        tag.setId(tagId);

        when(tagDAO.find(tagId)).thenReturn(tag);

        assertEquals(tag, tagService.findTag(tagId));
    }

    /**
     * Test tag service delete tag.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testTagServiceDeleteTag() throws DAOException, ServiceException {
        tagService.deleteTag(anyLong());
    }

    /**
     * Test tag service create tag.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testTagServiceCreateTag() throws DAOException, ServiceException {
        Tag tag = new Tag();
        Tag retTag = new Tag();

        Long tagId = anyLong();
        retTag.setId(tagId);

        when(tagDAO.create(tag)).thenReturn(retTag);

        tagService.addTag(tag);

        assertEquals(retTag.getId(), tag.getId());
    }

    /**
     * Test find news by tag.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testFindNewsByTag() throws DAOException, ServiceException {
        News news = new News();

        Long tagId = anyLong();
        Long newsId = tagId;
        news.setId(newsId);

        when(newsTagDAO.readNewsIdsByTagId(tagId)).
                thenReturn(Arrays.asList(newsId));
        when(newsService.findNews(newsId)).thenReturn(news);

        List<News> retNews = tagService.findNewsByTag(tagId);

        assertEquals(Arrays.asList(news), retNews);
    }

    /**
     * Test find tags by news.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testFindTagsByNews() throws DAOException, ServiceException {
        Tag tag = new Tag();

        Long tagId = anyLong();
        Long newsId = tagId;
        tag.setId(tagId);

        when(newsTagDAO.findTagIdsByNewsId(newsId)).
                thenReturn(Arrays.asList(tagId));
        when(tagDAO.find(tagId)).thenReturn(tag);

        List<Tag> retTags = tagService.findTagsByNewsId(newsId);

        assertEquals(Arrays.asList(tag), retTags);
    }
}
