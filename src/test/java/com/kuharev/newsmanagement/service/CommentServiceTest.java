package com.kuharev.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.anyLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.mockito.Mockito.when;

import java.util.List;

import static org.junit.Assert.*;

import com.kuharev.newsmanagement.dao.impl.CommentDAO;
import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.DataLoaderException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.impl.CommentServiceImpl;
import com.kuharev.newsmanagement.util.DataLoader;

/**
 * The Class CommentServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testBeans.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class CommentServiceTest {

    /**
     * Comment data loader bean.
     */
    @Autowired
    private DataLoader<Comment> commentDataLoader;

    /**
     * The comment dao.
     */
    @Mock
    private CommentDAO commentDAO;

    /**
     * The comment service.
     */
    @InjectMocks
    private CommentServiceImpl commentService;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Sets the comment data loader.
     *
     * @param commentDataLoader the new comment data loader
     */
    public void setCommentDataLoader(DataLoader<Comment> commentDataLoader) {
        this.commentDataLoader = commentDataLoader;
    }

    /**
     * Test find comment.
     *
     * @throws DAOException        the DAO exception
     * @throws ServiceException    the service exception
     * @throws DataLoaderException the data loader exception
     */
    @Test
    public void testFindComment() throws DAOException, ServiceException, DataLoaderException {
        List<Comment> expected = commentDataLoader.loadData("data/data.xml");
        when(commentDAO.findByNewsId(anyLong())).thenReturn(expected);

        assertEquals(expected, commentService.findCommentsByNewsId(anyLong()));
    }

    /**
     * Test delete comment.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testDeleteComment() throws DAOException, ServiceException {
        commentService.deleteComment(anyLong());
    }

    /**
     * Test create comment.
     *
     * @throws DAOException     the DAO exception
     * @throws ServiceException the service exception
     */
    @Test
    public void testCreateComment() throws DAOException, ServiceException {
        Comment comment = new Comment();
        Comment retComment = new Comment();

        Long commentId = anyLong();
        retComment.setId(commentId);

        when(commentDAO.create(anyLong(), comment)).thenReturn(retComment);

        commentService.addComment(commentId, comment);

        assertEquals(retComment.getId(), comment.getId());
    }
}
