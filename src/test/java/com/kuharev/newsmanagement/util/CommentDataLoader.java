package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Comment data loader.
 */
public class CommentDataLoader implements DataLoader<Comment> {

    /**
     * The date format.
     */
    private static final String DATE_FORMAT = "yy-MM-dd HH:mm:ss";

    /**
     * The db column comment_id.
     */
    private static final String DB_COLUMN_COMMENT_ID = "comment_id";

    /**
     * The db column comment_text.
     */
    private static final String DB_COLUMN_COMMENT_TEXT = "comment_text";

    /**
     * The db column creation_date.
     */
    private static final String DB_COLUMN_COMMENT_CREATION_DATE = "creation_date";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "comments";

    @Override
    public List<Comment> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            return parseDataSet(dataSet);
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Comment> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Comment> comments = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();
        DateFormat format = new SimpleDateFormat(DATE_FORMAT);

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();

            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long commentId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_COMMENT_ID).toString());
                    String commentText = tableIterator.getTable().
                            getValue(i, DB_COLUMN_COMMENT_TEXT).toString();
                    Timestamp creationDate = new Timestamp(format.parse(tableIterator.getTable().
                            getValue(i, DB_COLUMN_COMMENT_CREATION_DATE).toString()).getTime());

                    comments.add(new Comment(commentId, commentText, creationDate));
                }
            }
        }
        return comments;
    }
}
