package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Author data loader.
 */
public class AuthorDataLoader implements DataLoader<Author> {

    /**
     * The date format.
     */
    private static final String DATE_FORMAT = "yy-MM-dd HH:mm:ss";

    /**
     * The db column author_id.
     */
    private static final String DB_COLUMN_AUTHOR_ID = "author_id";

    /**
     * The db column author_name.
     */
    private static final String DB_COLUMN_AUTHOR_NAME = "author_name";

    /**
     * The db column expired.
     */
    private static final String DB_COLUMN_AUTHOR_EXPIRED = "expired";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "authors";

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.util.DataLoader#loadData(java.lang.String)
     */
    @Override
    public List<Author> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);

            return parseDataSet(dataSet);
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    /**
     * Parses the data set.
     *
     * @param dataSet the data set
     * @return the list
     * @throws DataSetException the data set exception
     * @throws ParseException   the parse exception
     */
    private List<Author> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Author> authors = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();
        DateFormat format = new SimpleDateFormat(DATE_FORMAT);

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();
            
            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long authorId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_AUTHOR_ID).toString());
                    String authorName = tableIterator.getTable().
                            getValue(i, DB_COLUMN_AUTHOR_NAME).toString();
                    Timestamp expired = new Timestamp(format.parse(tableIterator.getTable()
                            .getValue(i, DB_COLUMN_AUTHOR_EXPIRED).toString()).getTime());

                    authors.add(new Author(authorId, authorName, expired));
                }
            }
        }
        return authors;
    }


}