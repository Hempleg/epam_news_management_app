package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Tag data loader.
 */
public class TagDataLoader implements DataLoader<Tag> {

    /**
     * The db column tag_id
     */
    private static final String DB_COLUMN_TAG_ID = "tag_id";

    /**
     * The db column tag_name
     */
    private static final String DB_COLUMN_TAG_NAME = "tag_name";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "tags";

    public List<Tag> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            List<Tag> tags = parseDataSet(dataSet);
            return tags;
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Tag> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Tag> tags = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();

            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long tagId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_TAG_ID).toString());
                    String tagName = tableIterator.getTable().
                            getValue(i, DB_COLUMN_TAG_NAME).toString();

                    tags.add(new Tag(tagId, tagName));
                }
            }
        }
        return tags;
    }
}
