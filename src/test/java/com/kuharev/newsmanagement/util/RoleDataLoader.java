package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Tag data loader.
 */
public class RoleDataLoader implements DataLoader<Role> {

    /**
     * The db column role_id
     */
    private static final String DB_COLUMN_ROLE_ID = "role_id";

    /**
     * The db column role_name
     */
    private static final String DB_COLUMN_ROLE_NAME = "role_name";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "roles";

    public List<Role> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            List<Role> roles = parseDataSet(dataSet);

            return roles;
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Role> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Role> roles = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();

            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long roleId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_ROLE_ID).toString());
                    String roleName = tableIterator.getTable().
                            getValue(i, DB_COLUMN_ROLE_NAME).toString();

                    roles.add(new Role(roleId, roleName));
                }
            }
        }
        return roles;
    }
}
