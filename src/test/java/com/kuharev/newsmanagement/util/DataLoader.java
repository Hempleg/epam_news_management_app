package com.kuharev.newsmanagement.util;

import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import com.kuharev.newsmanagement.exeption.DataLoaderException;

/**
 * The Interface DataLoader.
 *
 * @param <T> the generic type
 */
public interface DataLoader<T> {

    /**
     * Load data.
     *
     * @param file the file
     * @return the list
     * @throws DataLoaderException the data loader exception
     */
    List<T> loadData(String file) throws DataLoaderException;

    /**
     * Gets the data set.
     *
     * @param file the file
     * @return the data set
     * @throws DataSetException the data set exception
     */
    default IDataSet getDataSet(String file) throws DataSetException {
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        builder.setColumnSensing(true);

        return builder.build(getClass().getClassLoader().getResourceAsStream(file));
    }
}
