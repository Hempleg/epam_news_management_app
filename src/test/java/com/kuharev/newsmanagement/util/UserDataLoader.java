package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * User data loader.
 */
public class UserDataLoader implements DataLoader<User> {

    /**
     * The db column user_id
     */
    private static final String DB_COLUMN_USER_ID = "user_id";

    /**
     * The db column user_name
     */
    private static final String DB_COLUMN_USER_NAME = "user_name";

    /**
     * The db column login
     */
    private static final String DB_COLUMN_USER_LOGIN = "login";

    /**
     * The db column password
     */
    private static final String DB_COLUMN_USER_PASSWORD = "password";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "users";

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.util.DataLoader#loadData(java.lang.String)
     */
    @Override
    public List<User> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);

            return parseDataSet(dataSet);
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    /**
     * Parses the data set.
     *
     * @param dataSet the data set
     * @return the list
     * @throws DataSetException the data set exception
     * @throws ParseException   the parse exception
     */
    private List<User> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<User> users = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();

            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long userId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_USER_ID).toString());
                    String userName = tableIterator.getTable().
                            getValue(i, DB_COLUMN_USER_NAME).toString();
                    String login = tableIterator.getTable().
                            getValue(i, DB_COLUMN_USER_LOGIN).toString();
                    String password = tableIterator.getTable().
                            getValue(i, DB_COLUMN_USER_PASSWORD).toString();

                    users.add(new User(userId, userName, login, password));
                }
            }
        }
        return users;
    }
}