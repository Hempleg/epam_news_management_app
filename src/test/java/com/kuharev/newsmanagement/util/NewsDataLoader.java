package com.kuharev.newsmanagement.util;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.exeption.DataLoaderException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * News data loader.
 */
public class NewsDataLoader implements DataLoader<News> {

    /**
     * The date format.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * The DB column news_id.
     */
    private static final String DB_COLUMN_NEWS_ID = "news_id";

    /**
     * The DB column title.
     */
    private static final String DB_COLUMN_NEWS_TITLE = "title";

    /**
     * The DB column short_text.
     */
    private static final String DB_COLUMN_NEWS_SHORT_TEXT = "short_text";

    /**
     * The DB column full_text.
     */
    private static final String DB_COLUMN_NEWS_FULL_TEXT = "full_text";

    /**
     * The DB column creation_date.
     */
    private static final String DB_COLUMN_NEWS_CREATION_DATE = "creation_date";

    /**
     * The DB column modification_date.
     */
    private static final String DB_COLUMN_NEWS_MODIFICATION_DATE = "modification_date";

    /**
     * The db table name.
     */
    private static final String DB_TABLE_NAME = "news";

    @Override
    public List<News> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            return parseDataSet(dataSet);
        } catch (DataSetException | ParseException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<News> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<News> newsList = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();

        DateFormat format = new SimpleDateFormat(DATE_FORMAT);
        format.setLenient(false);

        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();

            if (DB_TABLE_NAME.equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long newsId = Long.parseLong(tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_ID).toString());
                    String title = tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_TITLE).toString();
                    String shortText = tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_SHORT_TEXT).toString();
                    String fullText = tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_FULL_TEXT).toString();
                    Timestamp creationDate = new Timestamp(format.parse(tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_CREATION_DATE).toString()).getTime());
                    Timestamp modificationDate = new Timestamp(format.parse(tableIterator.getTable().
                            getValue(i, DB_COLUMN_NEWS_MODIFICATION_DATE).toString()).getTime());

                    newsList.add(new News(newsId, title, shortText, fullText, creationDate, modificationDate));
                }
            }
        }
        return newsList;
    }
}