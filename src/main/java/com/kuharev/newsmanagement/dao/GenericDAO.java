package com.kuharev.newsmanagement.dao;

import java.io.Serializable;

import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Interface GenericDAO.
 *
 * @param <T>  the generic type
 * @param <PK> the generic type
 */
public interface GenericDAO<T, PK extends Serializable> {

    /**
     * Creates the entity.
     *
     * @param entity the entity
     * @return the t
     * @throws DAOException the data access db exception
     */
    T create(T entity) throws DAOException;

    /**
     * Read.
     *
     * @param id the id
     * @return the t
     * @throws DAOException the data access db exception
     */
    T find(PK id) throws DAOException;

    /**
     * Update.
     *
     * @param entity the entity
     * @throws DAOException the data access db exception
     */
    void update(T entity) throws DAOException;

    /**
     * Delete.
     *
     * @param id the id
     * @throws DAOException the data access db exception
     */
    void delete(PK id) throws DAOException;
}
