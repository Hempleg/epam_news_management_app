package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class CommentDAO.
 */
public class CommentDAO implements GenericDAO<Comment, Long> {

    /**
     * The db column comemnt_id.
     */
    private static final String DB_COLUMN_COMMENT_ID = "comment_id";

    /**
     * The db column news_id.
     */
    private static final String DB_COLUMN_NEWS_ID = "news_id";

    /**
     * The db column comment_text.
     */
    private static final String DB_COLUMN_COMMENT_TEXT = "comment_text";

    /**
     * The db column creation_date.
     */
    private static final String DB_COLUMN_COMMENT_CREATION_DATE = "creation_date";

    /**
     * The sql create comment.
     */
    private final String SQL_CREATE_COMMENT;

    /**
     * The sql find comment.
     */
    private final String SQL_READ_COMMENT;

    /**
     * The sql update comment.
     */
    private final String SQL_UPDATE_COMMENT;

    /**
     * The sql delete comment.
     */
    private final String SQL_DELETE_COMMENT;

    /**
     * The sql find all comments for news.
     */
    private final String SQL_READ_ALL_COMMENTS_FOR_NEWS;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new comment dao.
     *
     * @param sqlCreate         the sql create
     * @param sqlRead           the sql find
     * @param sqlUpdate         the sql update
     * @param sqlDelete         the sql delete
     * @param sqlReadAllForNews the sql find all for news
     */
    private CommentDAO(String sqlCreate,
                       String sqlRead,
                       String sqlUpdate,
                       String sqlDelete,
                       String sqlReadAllForNews) {
        this.SQL_CREATE_COMMENT = sqlCreate;
        this.SQL_READ_COMMENT = sqlRead;
        this.SQL_UPDATE_COMMENT = sqlUpdate;
        this.SQL_DELETE_COMMENT = sqlDelete;
        this.SQL_READ_ALL_COMMENTS_FOR_NEWS = sqlReadAllForNews;
    }

    /**
     * Read by news id.
     *
     * @param newsId the news id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Comment> findByNewsId(Long newsId) throws DAOException {
        List<Comment> commentList = new ArrayList<Comment>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_ALL_COMMENTS_FOR_NEWS);
        ) {
            statement.setLong(1, newsId);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Comment comment = new Comment();
                generateComment(rs, comment);

                commentList.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return commentList;
    }

    /**
     * Creates the comment.
     *
     * @param newsId the news id
     * @param entity the entity
     * @return the comment
     * @throws DAOException the DAO exception
     */
    public Comment create(Long newsId, Comment entity) throws DAOException {
        Comment comment = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_COMMENT,
                        new String[]{DB_COLUMN_COMMENT_ID});
        ) {
            statement.setLong(1, newsId);
            statement.setString(2, entity.getText());
            statement.setTimestamp(3, entity.getCreationDate());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            comment = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return comment;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public Comment find(Long id) throws DAOException {
        Comment comment = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_COMMENT);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                comment = new Comment();
                generateComment(rs, comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return comment;
    }

    /**
     * Generate comment.
     *
     * @param rs      the rs
     * @param comment the comment
     * @throws SQLException the SQL exception
     */
    private void generateComment(ResultSet rs, Comment comment) throws SQLException {
        comment.setId(rs.getLong(DB_COLUMN_COMMENT_ID));
        comment.setText(rs.getString(DB_COLUMN_COMMENT_TEXT));
        comment.setCreationDate(rs.getTimestamp(DB_COLUMN_COMMENT_CREATION_DATE));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(Comment entity) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_COMMENT,
                        new String[]{DB_COLUMN_COMMENT_ID});
        ) {
            statement.setString(1, entity.getText());
            statement.setTimestamp(2, entity.getCreationDate());
            statement.setLong(3, entity.getId());

            statement.executeUpdate();


        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COMMENT);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public Comment create(Comment entity) throws DAOException {
        throw new UnsupportedOperationException("Use method create(News, Comment) instead!");
    }

}
