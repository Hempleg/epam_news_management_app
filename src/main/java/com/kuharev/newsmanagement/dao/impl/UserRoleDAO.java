package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class UserRoleDAO.
 */
public class UserRoleDAO {

    /**
     * The db columnn role_id
     */
    private static final String DB_COLUMN_ROLE_ID = "role_id";

    /**
     * The db column user_id
     */
    private static final String DB_COLUMN_USER_ID = "user_id";

    /**
     * The sql create userrole.
     */
    private final String SQL_CREATE_USERROLE;

    /**
     * The sql find userrole by user id.
     */
    private final String SQL_READ_USERROLE_BY_USER_ID;

    /**
     * The sql find userids by role id.
     */
    private final String SQL_READ_USERIDS_BY_ROLE_ID;

    /**
     * The sql delete userrole.
     */
    private final String SQL_DELETE_USERROLE;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new user role dao.
     *
     * @param sqlCreate     the sql create
     * @param sqlReadByUser the sql find by user
     * @param sqlReadByRole the sql find by role
     * @param sqlDelete     the sql delete
     */
    private UserRoleDAO(String sqlCreate,
                        String sqlReadByUser,
                        String sqlReadByRole,
                        String sqlDelete) {
        this.SQL_CREATE_USERROLE = sqlCreate;
        this.SQL_READ_USERROLE_BY_USER_ID = sqlReadByUser;
        this.SQL_READ_USERIDS_BY_ROLE_ID = sqlReadByRole;
        this.SQL_DELETE_USERROLE = sqlDelete;
    }

    /**
     * Creates the.
     *
     * @param userId the user id
     * @param roleId the role id
     * @throws DAOException the DAO exception
     */
    public void create(Long userId, Long roleId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_USERROLE);
        ) {
            statement.setLong(1, userId);
            statement.setLong(2, roleId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Read role id by user id.
     *
     * @param userId the user id
     * @return the long
     * @throws DAOException the DAO exception
     */
    public Long readRoleIdByUserId(Long userId) throws DAOException {
        Long roleId = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_USERROLE_BY_USER_ID);
        ) {
            statement.setLong(1, userId);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                roleId = rs.getLong(DB_COLUMN_ROLE_ID);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return roleId;
    }


    /**
     * Read user ids by role id.
     *
     * @param roleId the role id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Long> readUserIdsByRoleId(Long roleId) throws DAOException {
        List<Long> userIds = new ArrayList<Long>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_USERIDS_BY_ROLE_ID);
        ) {
            statement.setLong(1, roleId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                userIds.add(rs.getLong(DB_COLUMN_USER_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return userIds;
    }

    /**
     * Delete.
     *
     * @param userId the user id
     * @param roleId the role id
     * @throws DAOException the DAO exception
     */
    public void delete(Long userId, Long roleId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USERROLE);
        ) {
            statement.setLong(1, userId);
            statement.setLong(2, roleId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
