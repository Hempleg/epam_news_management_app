package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class RoleDAO.
 */
public class RoleDAO implements GenericDAO<Role, Long> {
    /**
     * The db column role_id
     */
    private static final String DB_COLUMN_ROLE_ID = "role_id";

    /**
     * The db column role_name
     */
    private static final String DB_COLUMN_ROLE_NAME = "role_name";

    /**
     * The sql create role.
     */
    private final String SQL_CREATE_ROLE;

    /**
     * The sql find role.
     */
    private final String SQL_READ_ROLE;

    /**
     * The sql update role.
     */
    private final String SQL_UPDATE_ROLE;

    /**
     * The sql delete role.
     */
    private final String SQL_DELETE_ROLE;

    /**
     * The sql find all roles.
     */
    private final String SQL_READ_ALL_ROLES;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new role dao.
     *
     * @param sqlCreate  the sql create
     * @param sqlRead    the sql find
     * @param sqlUpdate  the sql update
     * @param sqlDelete  the sql delete
     * @param sqlReadAll the sql find all
     */
    private RoleDAO(String sqlCreate,
                    String sqlRead,
                    String sqlUpdate,
                    String sqlDelete,
                    String sqlReadAll) {
        this.SQL_CREATE_ROLE = sqlCreate;
        this.SQL_READ_ROLE = sqlRead;
        this.SQL_UPDATE_ROLE = sqlUpdate;
        this.SQL_DELETE_ROLE = sqlDelete;
        this.SQL_READ_ALL_ROLES = sqlReadAll;
    }

    /**
     * Read all.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Role> readAll() throws DAOException {
        List<Role> roleList = new ArrayList<Role>();

        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery(SQL_READ_ALL_ROLES);

            while (rs.next()) {
                Role role = new Role();
                generateRole(rs, role);

                roleList.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return roleList;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public Role create(Role entity) throws DAOException {
        Role role = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_ROLE,
                        new String[]{DB_COLUMN_ROLE_ID});
        ) {
            statement.setString(1, entity.getName());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            role = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return role;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public Role find(Long id) throws DAOException {
        Role role = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_ROLE);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                role = new Role();
                generateRole(rs, role);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return role;
    }

    /**
     * Generate role.
     *
     * @param rs   the rs
     * @param role the role
     * @throws SQLException the SQL exception
     */
    private void generateRole(ResultSet rs, Role role) throws SQLException {
        role.setId(rs.getLong(DB_COLUMN_ROLE_ID));
        role.setName(rs.getString(DB_COLUMN_ROLE_NAME));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(Role entity) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ROLE,
                        new String[]{DB_COLUMN_ROLE_ID});
        ) {
            statement.setString(1, entity.getName());
            statement.setLong(2, entity.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ROLE);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


}
