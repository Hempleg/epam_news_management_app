package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class UserDAO.
 */
public class UserDAO implements GenericDAO<User, Long> {

    /**
     * The db column user_id
     */
    private static final String DB_COLUMN_USER_ID = "user_id";

    /**
     * The db column user_name
     */
    private static final String DB_COLUMN_USER_NAME = "user_name";

    /**
     * The db column login
     */
    private static final String DB_COLUMN_USER_LOGIN = "login";

    /**
     * The db column password
     */
    private static final String DB_COLUMN_USER_PASSWORD = "password";

    /**
     * The sql create user.
     */
    private final String SQL_CREATE_USER;

    /**
     * The sql find user.
     */
    private final String SQL_READ_USER;

    /**
     * The sql update user.
     */
    private final String SQL_UPDATE_USER;

    /**
     * The sql delete user.
     */
    private final String SQL_DELETE_USER;

    /**
     * The sql find all users.
     */
    private final String SQL_READ_ALL_USERS;

    /**
     * The data source.
     */
    private BasicDataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public BasicDataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new user dao.
     *
     * @param sqlCreate  the sql create
     * @param sqlRead    the sql find
     * @param sqlUpdate  the sql update
     * @param sqlDelete  the sql delete
     * @param sqlReadAll the sql find all
     */
    private UserDAO(String sqlCreate,
                    String sqlRead,
                    String sqlUpdate,
                    String sqlDelete,
                    String sqlReadAll) {
        this.SQL_CREATE_USER = sqlCreate;
        this.SQL_READ_USER = sqlRead;
        this.SQL_UPDATE_USER = sqlUpdate;
        this.SQL_DELETE_USER = sqlDelete;
        this.SQL_READ_ALL_USERS = sqlReadAll;
    }

    /**
     * Read all.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<User> readAll() throws DAOException {
        List<User> userList = new ArrayList<User>();

        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery(SQL_READ_ALL_USERS);

            while (rs.next()) {
                User user = new User();
                generateUser(rs, user);

                userList.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return userList;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public User create(User entity) throws DAOException {
        User user = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_USER,
                        new String[]{DB_COLUMN_USER_ID});
        ) {
            statement.setString(1, entity.getUserName());
            statement.setString(2, entity.getLogin());
            statement.setString(3, entity.getPassword());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            user = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return user;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public User find(Long id) throws DAOException {
        User user = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_USER);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                user = new User();
                generateUser(rs, user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return user;
    }

    /**
     * Generate user.
     *
     * @param rs   the rs
     * @param user the user
     * @throws SQLException the SQL exception
     */
    private void generateUser(ResultSet rs, User user) throws SQLException {
        user.setId(rs.getLong(DB_COLUMN_USER_ID));
        user.setUserName(rs.getString(DB_COLUMN_USER_NAME));
        user.setLogin(rs.getString(DB_COLUMN_USER_LOGIN));
        user.setPassword(rs.getString(DB_COLUMN_USER_PASSWORD));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(User entity) throws DAOException {

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER,
                        new String[]{DB_COLUMN_USER_ID});
        ) {
            statement.setString(1, entity.getUserName());
            statement.setString(2, entity.getLogin());
            statement.setString(3, entity.getPassword());
            statement.setLong(4, entity.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

}
