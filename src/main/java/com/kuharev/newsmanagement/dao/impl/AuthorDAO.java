package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class AuthorDAO.
 */
public class AuthorDAO implements GenericDAO<Author, Long> {

    /**
     * The db column author_id.
     */
    private static final String DB_COLUMN_AUTHOR_ID = "author_id";

    /**
     * The db column author_name.
     */
    private static final String DB_COLUMN_AUTHOR_NAME = "author_name";

    /**
     * The db column expired.
     */
    private static final String DB_COLUMN_AUTHOR_EXPIRED = "expired";

    /**
     * The sql create author.
     */
    private final String SQL_CREATE_AUTHOR;

    /**
     * The sql find author.
     */
    private final String SQL_READ_AUTHOR;

    /**
     * The sql update author.
     */
    private final String SQL_UPDATE_AUTHOR;

    /**
     * The sql delete author.
     */
    private final String SQL_DELETE_AUTHOR;

    /**
     * The sql find all authors.
     */
    private final String SQL_READ_ALL_AUTHORS;

    /**
     * The data source.
     */
    private DataSource dataSource;


    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new author dao.
     *
     * @param sqlCreate  the sql create
     * @param sqlRead    the sql find
     * @param sqlUpdate  the sql update
     * @param sqlDelete  the sql delete
     * @param sqlReadAll the sql find all
     */
    private AuthorDAO(String sqlCreate,
                      String sqlRead,
                      String sqlUpdate,
                      String sqlDelete,
                      String sqlReadAll) {
        this.SQL_CREATE_AUTHOR = sqlCreate;
        this.SQL_READ_AUTHOR = sqlRead;
        this.SQL_UPDATE_AUTHOR = sqlUpdate;
        this.SQL_DELETE_AUTHOR = sqlDelete;
        this.SQL_READ_ALL_AUTHORS = sqlReadAll;
    }

    /**
     * Read all.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Author> findAll() throws DAOException {
        List<Author> authorList = new ArrayList<Author>();

        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery(SQL_READ_ALL_AUTHORS);

            while (rs.next()) {
                Author author = new Author();
                generateAuthor(rs, author);

                authorList.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return authorList;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public Author create(Author entity) throws DAOException {
        Author author = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_AUTHOR,
                        new String[]{DB_COLUMN_AUTHOR_ID});
        ) {
            statement.setString(1, entity.getName());
            statement.setTimestamp(2, entity.getExpired());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            author = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return author;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public Author find(Long id) throws DAOException {
        Author author = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_AUTHOR);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                author = new Author();
                generateAuthor(rs, author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return author;
    }

    /**
     * Generate author.
     *
     * @param rs     the rs
     * @param author the author
     * @throws SQLException the SQL exception
     */
    private void generateAuthor(ResultSet rs, Author author) throws SQLException {
        author.setId(rs.getLong(DB_COLUMN_AUTHOR_ID));
        author.setName(rs.getString(DB_COLUMN_AUTHOR_NAME));
        author.setExpired(rs.getTimestamp(DB_COLUMN_AUTHOR_EXPIRED));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(Author entity) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_AUTHOR,
                        new String[]{DB_COLUMN_AUTHOR_ID});
        ) {
            statement.setString(1, entity.getName());
            statement.setTimestamp(2, entity.getExpired());
            statement.setLong(3, entity.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

}
