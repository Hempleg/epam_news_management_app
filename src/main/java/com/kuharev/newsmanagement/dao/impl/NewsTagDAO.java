package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class NewsTagDAO.
 */
public class NewsTagDAO {

    /**
     * The db column news_id.
     */
    private static final String DB_COLUMN_NEWS_ID = "news_id";

    /**
     * The db column tag_id
     */
    private static final String DB_COLUMN_TAG_ID = "tag_id";

    /**
     * The sql create newstag.
     */
    private final String SQL_CREATE_NEWSTAG;

    /**
     * The sql find newstags by news id.
     */
    private final String SQL_READ_NEWSTAGS_BY_NEWS_ID;

    /**
     * The sql find newsids by tag id.
     */
    private final String SQL_READ_NEWSIDS_BY_TAG_ID;

    /**
     * The sql delete newstag.
     */
    private final String SQL_DELETE_NEWSTAG;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new news tag dao.
     *
     * @param sqlCreate     the sql create
     * @param sqlReadByNews the sql find by news
     * @param sqlReadByTag  the sql find by tag
     * @param sqlDelete     the sql delete
     */
    private NewsTagDAO(String sqlCreate,
                       String sqlReadByNews,
                       String sqlReadByTag,
                       String sqlDelete) {
        this.SQL_CREATE_NEWSTAG = sqlCreate;
        this.SQL_READ_NEWSIDS_BY_TAG_ID = sqlReadByTag;
        this.SQL_READ_NEWSTAGS_BY_NEWS_ID = sqlReadByNews;
        this.SQL_DELETE_NEWSTAG = sqlDelete;
    }

    /**
     * Creates the.
     *
     * @param newsId the news id
     * @param tagId  the tag id
     * @throws DAOException the DAO exception
     */
    public void create(Long newsId, Long tagId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_NEWSTAG);
        ) {
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Read tag ids by news id.
     *
     * @param newsId the news id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Long> findTagIdsByNewsId(Long newsId) throws DAOException {
        List<Long> tagsIds = new ArrayList<Long>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_NEWSTAGS_BY_NEWS_ID);
        ) {
            statement.setLong(1, newsId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tagsIds.add(rs.getLong(DB_COLUMN_TAG_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tagsIds;
    }


    /**
     * Read news ids by tag id.
     *
     * @param tagId the tag id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Long> readNewsIdsByTagId(Long tagId) throws DAOException {
        List<Long> newsIds = new ArrayList<Long>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_NEWSIDS_BY_TAG_ID);
        ) {
            statement.setLong(1, tagId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                newsIds.add(rs.getLong(DB_COLUMN_NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return newsIds;
    }

    /**
     * Delete the entity.
     *
     * @param newsId the news id
     * @param tagId  the tag id
     * @throws DAOException the DAO exception
     */
    public void delete(Long newsId, Long tagId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_NEWSTAG);
        ) {
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
