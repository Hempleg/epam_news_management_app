package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class NewsAuthorDAO.
 */
public class NewsAuthorDAO {

    /**
     * The DB column author_id.
     */
    private static final String DB_COLUMN_AUTHOR_ID = "author_id";
    /**
     * The DB column news_id.
     */
    private static final String DB_COLUMN_NEWS_ID = "news_id";
    /**
     * The sql create newsauthor.
     */
    private final String SQL_CREATE_NEWSAUTHOR;

    /**
     * The sql find newsauthors by news id.
     */
    private final String SQL_READ_NEWSAUTHORS_BY_NEWS_ID;

    /**
     * The sql find newsids by author id.
     */
    private final String SQL_READ_NEWSIDS_BY_AUTHOR_ID;

    /**
     * The sql delete newsauthor.
     */
    private final String SQL_DELETE_NEWSAUTHOR;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new news author dao.
     *
     * @param sqlCreate       the sql create
     * @param sqlReadByNews   the sql find by news
     * @param sqlReadByAuthor the sql find by author
     * @param sqlDelete       the sql delete
     */
    private NewsAuthorDAO(String sqlCreate,
                          String sqlReadByNews,
                          String sqlReadByAuthor,
                          String sqlDelete) {
        this.SQL_CREATE_NEWSAUTHOR = sqlCreate;
        this.SQL_READ_NEWSIDS_BY_AUTHOR_ID = sqlReadByAuthor;
        this.SQL_READ_NEWSAUTHORS_BY_NEWS_ID = sqlReadByNews;
        this.SQL_DELETE_NEWSAUTHOR = sqlDelete;
    }

    /**
     * Creates the.
     *
     * @param newsId   the news id
     * @param authorId the author id
     * @throws DAOException the DAO exception
     */
    public void create(Long newsId, Long authorId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_NEWSAUTHOR);
        ) {
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Read author ids by news id.
     *
     * @param newsId the news id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Long> findAuthorIdsByNewsId(Long newsId) throws DAOException {
        List<Long> authorsIds = new ArrayList<Long>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_NEWSAUTHORS_BY_NEWS_ID);
        ) {
            statement.setLong(1, newsId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                authorsIds.add(rs.getLong(DB_COLUMN_AUTHOR_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return authorsIds;
    }


    /**
     * Read news ids by author id.
     *
     * @param authorId the author id
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Long> readNewsIdsByAuthorId(Long authorId) throws DAOException {
        List<Long> newsIds = new ArrayList<Long>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_NEWSIDS_BY_AUTHOR_ID);
        ) {
            statement.setLong(1, authorId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                newsIds.add(rs.getLong(DB_COLUMN_NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return newsIds;
    }

    /**
     * Delete.
     *
     * @param newsId   the news id
     * @param authorId the author id
     * @throws DAOException the DAO exception
     */
    public void delete(Long newsId, Long authorId) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_NEWSAUTHOR);
        ) {
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
