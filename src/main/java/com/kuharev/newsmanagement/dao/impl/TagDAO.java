package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;

/**
 * The Class TagDAO.
 */
public class TagDAO implements GenericDAO<Tag, Long> {
    /**
     * The db column tag_id
     */
    private static final String DB_COLUMN_TAG_ID = "tag_id";

    /**
     * The db column tag_name
     */
    private static final String DB_COLUMN_TAG_NAME = "tag_name";

    /**
     * The sql create tag.
     */
    private final String SQL_CREATE_TAG;

    /**
     * The sql find tag.
     */
    private final String SQL_READ_TAG;

    /**
     * The sql update tag.
     */
    private final String SQL_UPDATE_TAG;

    /**
     * The sql delete tag.
     */
    private final String SQL_DELETE_TAG;

    /**
     * The sql find all tags.
     */
    private final String SQL_READ_ALL_TAGS;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new tag dao.
     *
     * @param sqlCreate  the sql create
     * @param sqlRead    the sql find
     * @param sqlUpdate  the sql update
     * @param sqlDelete  the sql delete
     * @param sqlReadAll the sql find all
     */
    private TagDAO(String sqlCreate,
                   String sqlRead,
                   String sqlUpdate,
                   String sqlDelete,
                   String sqlReadAll) {
        this.SQL_CREATE_TAG = sqlCreate;
        this.SQL_READ_TAG = sqlRead;
        this.SQL_UPDATE_TAG = sqlUpdate;
        this.SQL_DELETE_TAG = sqlDelete;
        this.SQL_READ_ALL_TAGS = sqlReadAll;
    }

    /**
     * Read all.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<Tag> readAll() throws DAOException {
        List<Tag> tagList = new ArrayList<Tag>();

        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery(SQL_READ_ALL_TAGS);

            while (rs.next()) {
                Tag tag = new Tag();
                generateTag(rs, tag);

                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tagList;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public Tag create(Tag entity) throws DAOException {
        Tag tag = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CREATE_TAG,
                        new String[]{DB_COLUMN_TAG_ID});
        ) {
            statement.setString(1, entity.getName());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            tag = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tag;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public Tag find(Long id) throws DAOException {
        Tag tag = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_TAG);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                tag = new Tag();
                generateTag(rs, tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tag;
    }

    /**
     * Generate tag.
     *
     * @param rs  the rs
     * @param tag the tag
     * @throws SQLException the SQL exception
     */
    private void generateTag(ResultSet rs, Tag tag) throws SQLException {
        tag.setId(rs.getLong(DB_COLUMN_TAG_ID));
        tag.setName(rs.getString(DB_COLUMN_TAG_NAME));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(Tag entity) throws DAOException {
        Tag tag = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TAG,
                        new String[]{DB_COLUMN_TAG_ID});
        ) {
            statement.setString(1, entity.getName());
            statement.setLong(2, entity.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_TAG);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

}
