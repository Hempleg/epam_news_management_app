package com.kuharev.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kuharev.newsmanagement.dao.GenericDAO;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.util.SerchUtil;

/**
 * The Class NewsDAO.
 */
public class NewsDAO implements GenericDAO<News, Long> {

    /**
     * The DB column news_id.
     */
    private static final String DB_COLUMN_NEWS_ID = "news_id";

    /**
     * The DB column title.
     */
    private static final String DB_COLUMN_NEWS_TITLE = "title";

    /**
     * The DB column short_text.
     */
    private static final String DB_COLUMN_NEWS_SHORT_TEXT = "short_text";

    /**
     * The DB column full_text.
     */
    private static final String DB_COLUMN_NEWS_FULL_TEXT = "full_text";

    /**
     * The DB column creation_date.
     */
    private static final String DB_COLUMN_NEWS_CREATION_DATE = "creation_date";

    /**
     * The DB column modification_date.
     */
    private static final String DB_COLUMN_NEWS_MODIFICATION_DATE = "modification_date";

    /**
     * The sql create news.
     */
    private final String SQL_CREATE_NEWS;

    /**
     * The sql find news.
     */
    private final String SQL_READ_NEWS;

    /**
     * The sql update news.
     */
    private final String SQL_UPDATE_NEWS;

    /**
     * The sql delete news.
     */
    private final String SQL_DELETE_NEWS;

    /**
     * The sql find all news.
     */
    private final String SQL_READ_ALL_NEWS;

    /**
     * The data source.
     */
    private DataSource dataSource;

    /**
     * Gets the data source.
     *
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     *
     * @param dataSource the new data source
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Instantiates a new news dao.
     *
     * @param sqlCreate  the sql create
     * @param sqlRead    the sql find
     * @param sqlUpdate  the sql update
     * @param sqlDelete  the sql delete
     * @param sqlReadAll the sql find all
     */
    private NewsDAO(String sqlCreate,
                    String sqlRead,
                    String sqlUpdate,
                    String sqlDelete,
                    String sqlReadAll) {
        this.SQL_CREATE_NEWS = sqlCreate;
        this.SQL_READ_NEWS = sqlRead;
        this.SQL_UPDATE_NEWS = sqlUpdate;
        this.SQL_DELETE_NEWS = sqlDelete;
        this.SQL_READ_ALL_NEWS = sqlReadAll;
    }

    /**
     * Read all.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<News> findAll() throws DAOException {
        List<News> newsList = new ArrayList<News>();

        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery(SQL_READ_ALL_NEWS);

            while (rs.next()) {
                News news = new News();
                generateNews(rs, news);

                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return newsList;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#create(java.lang.Object)
     */
    @Override
    public News create(News entity) throws DAOException {
        News news = null;

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement =
                        connection.prepareStatement(SQL_CREATE_NEWS,
                                new String[]{DB_COLUMN_NEWS_ID});
        ) {
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getShortText());
            statement.setString(3, entity.getFullText());
            statement.setTimestamp(4, entity.getCreationDate());
            statement.setTimestamp(5, entity.getModificationDate());

            statement.executeUpdate();

            Long id = null;
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }

            news = find(id);

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return news;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#find(java.io.Serializable)
     */
    @Override
    public News find(Long id) throws DAOException {
        News news = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_READ_NEWS);
        ) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                news = new News();
                generateNews(rs, news);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return news;
    }

    /**
     * Generate news.
     *
     * @param rs   the rs
     * @param news the news
     * @throws SQLException the SQL exception
     */
    private void generateNews(ResultSet rs, News news) throws SQLException {
        news.setId(rs.getLong(DB_COLUMN_NEWS_ID));
        news.setTitle(rs.getString(DB_COLUMN_NEWS_TITLE));
        news.setShortText(rs.getString(DB_COLUMN_NEWS_SHORT_TEXT));
        news.setFullText(rs.getString(DB_COLUMN_NEWS_FULL_TEXT));
        news.setCreationDate(rs.getTimestamp(DB_COLUMN_NEWS_CREATION_DATE));
        news.setModificationDate(rs.getTimestamp(DB_COLUMN_NEWS_MODIFICATION_DATE));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#update(java.lang.Object)
     */
    @Override
    public void update(News entity) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_NEWS);
        ) {
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getShortText());
            statement.setString(3, entity.getFullText());
            statement.setTimestamp(4, entity.getCreationDate());
            statement.setTimestamp(5, entity.getModificationDate());
            statement.setLong(6, entity.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_DELETE_NEWS);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    /**
     * Find by serch criteria.
     *
     * @param sc the sc
     * @return the list
     * @throws DAOException the DAO exception
     */
    public List<News> findBySerchCriteria(SerchCriteria sc) throws DAOException {
        List<News> newsList = new ArrayList<News>();
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SerchUtil.buildQuery(sc));
        ) {
            SerchUtil.fillPreparedStatement(sc, statement);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                News news = new News();
                generateNews(rs, news);

                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return newsList;
    }
}
