package com.kuharev.newsmanagement.domain;

// TODO: Auto-generated Javadoc

/**
 * The Class User.
 */
public class User extends Entity {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The user name.
     */
    private String userName;

    /**
     * The login.
     */
    private String login;

    /**
     * The password.
     */
    private String password;

    /**
     * The role.
     */
    private Role role;

    /**
     * Instantiates a new user.
     */
    public User() {
    }

    /**
     * Instantiates a new user.
     *
     * @param userName the user name
     * @param login    the login
     * @param password the password
     * @param role     the role
     */
    public User(String userName, String login, String password, Role role) {
        super();
        this.userName = userName;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    /**
     * Instantiates a new user.
     *
     * @param id       the id
     * @param userName the user name
     * @param login    the login
     * @param password the password
     * @param role     the role
     */
    public User(Long id, String userName, String login, String password) {
        super(id);
        this.userName = userName;
        this.login = login;
        this.password = password;
    }

    /**
     * Gets the role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets the role.
     *
     * @param role the new role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userName the new user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login.
     *
     * @param login the new login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "User [userName=" + userName + ", login=" + login + ", password=" + password + "]";
    }


}
