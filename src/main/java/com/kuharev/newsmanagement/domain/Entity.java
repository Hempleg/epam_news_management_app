package com.kuharev.newsmanagement.domain;

import java.io.Serializable;

/**
 * The Class Entity.
 */
public class Entity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    private long id;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Instantiates a new entity.
     *
     * @param id the id
     */
    public Entity(long id) {
        super();
        this.id = id;
    }

    /**
     * Instantiates a new entity.
     */
    public Entity() {
        super();
    }

}
