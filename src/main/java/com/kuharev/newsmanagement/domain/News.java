package com.kuharev.newsmanagement.domain;

import java.sql.Timestamp;
import java.util.List;

/**
 * The Class News.
 */
public class News extends Entity {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The title.
     */
    private String title;

    /**
     * The short text.
     */
    private String shortText;

    /**
     * The full text.
     */
    private String fullText;

    /**
     * The creation date.
     */
    private Timestamp creationDate;

    /**
     * The modification date.
     */
    private Timestamp modificationDate;

    /**
     * The author.
     */
    private Author author;

    /**
     * The tags.
     */
    private List<Tag> tags;

    /**
     * Instantiates a new news.
     */
    public News() {
    }

    /**
     * Instantiates a new news.
     *
     * @param title            the title
     * @param shortText        the short text
     * @param fullText         the full text
     * @param creationDate     the creation date
     * @param modificationDate the modification date
     * @param author           the author
     * @param tags             the tags
     * @param comments         the comments
     */
    public News(String title, String shortText, String fullText, Timestamp creationDate, Timestamp modificationDate,
                Author author, List<Tag> tags, List<Comment> comments) {
        super();
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.author = author;
        this.tags = tags;
        this.comments = comments;
    }

    /**
     * Instantiates a new news.
     *
     * @param id               the id
     * @param title            the title
     * @param shortText        the short text
     * @param fullText         the full text
     * @param creationDate     the creation date
     * @param modificationDate the modification date
     */
    public News(long id, String title, String shortText, String fullText, Timestamp creationDate, Timestamp modificationDate) {
        super(id);
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    /**
     * The comments.
     */
    private List<Comment> comments;

    /**
     * Gets the comments.
     *
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Sets the comments.
     *
     * @param comments the new comments
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * Gets the tags.
     *
     * @return the tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     *
     * @param tags the new tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Gets the author.
     *
     * @return the author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets the author.
     *
     * @param author the new author
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the short text.
     *
     * @return the short text
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * Sets the short text.
     *
     * @param shortText the new short text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * Gets the full text.
     *
     * @return the full text
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * Sets the full text.
     *
     * @param fullText the new full text
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     *
     * @param creationDate the new creation date
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the modification date.
     *
     * @return the modification date
     */
    public Timestamp getModificationDate() {
        return modificationDate;
    }

    /**
     * Sets the modification date.
     *
     * @param modificationDate the new modification date
     */
    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }


}
