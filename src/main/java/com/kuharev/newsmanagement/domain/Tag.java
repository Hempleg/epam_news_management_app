package com.kuharev.newsmanagement.domain;

/**
 * The Class Tag.
 */
public class Tag extends Entity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * The name.
     */
    private String name;

    /**
     * Instantiates a new tag.
     */
    public Tag() {
    }

    /**
     * Instantiates a new tag.
     *
     * @param name the name
     */
    public Tag(String name) {
        super();
        this.name = name;
    }

    public Tag(Long id, String name) {
        super(id);
        this.name = name;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

}
