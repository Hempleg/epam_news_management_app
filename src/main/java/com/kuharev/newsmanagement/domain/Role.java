package com.kuharev.newsmanagement.domain;

// TODO: Auto-generated Javadoc

/**
 * The Class Role.
 */
public class Role extends Entity {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The name.
     */
    private String name;

    /**
     * Instantiates a new role.
     */
    public Role() {
    }

    /**
     * Instantiates a new role.
     *
     * @param name the name
     */
    public Role(String name) {
        super();
        this.name = name;
    }

    /**
     * Instantiates a new role.
     *
     * @param id   the id
     * @param name the name
     */
    public Role(long id, String name) {
        super(id);
        this.name = name;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

}
