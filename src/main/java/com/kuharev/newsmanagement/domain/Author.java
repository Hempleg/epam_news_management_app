package com.kuharev.newsmanagement.domain;

import java.sql.Timestamp;

/**
 * The Class Author.
 */
public class Author extends Entity {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The name.
     */
    private String name;

    /**
     * The expired.
     */
    private Timestamp expired;

    /**
     * Instantiates a new author.
     */
    public Author() {
    }

    /**
     * Instantiates a new author.
     *
     * @param name    the name
     * @param expired the expired
     */
    public Author(String name, Timestamp expired) {
        super();
        this.name = name;
        this.expired = expired;
    }


    /**
     * Instantiates a new author.
     *
     * @param id      the id
     * @param name    the name
     * @param expired the expired
     */
    public Author(Long id, String name, Timestamp expired) {
        super(id);
        this.name = name;
        this.expired = expired;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the expired.
     *
     * @return the expired
     */
    public Timestamp getExpired() {
        return expired;
    }

    /**
     * Sets the expired.
     *
     * @param expired the new expired
     */
    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Author [name=" + name + ", expired=" + expired + "]";
    }

}
