package com.kuharev.newsmanagement.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SerchCriteria.
 */
public class SerchCriteria implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The author.
     */
    private Author author;

    /**
     * The tags.
     */
    private List<Tag> tags;

    /**
     * Gets the author.
     *
     * @return the author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets the author.
     *
     * @param author the new author
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Gets the tags.
     *
     * @return the tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     *
     * @param tags the new tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Instantiates a new serch criteria.
     *
     * @param author the author
     * @param tags   the tags
     */
    public SerchCriteria(Author author, List<Tag> tags) {
        super();
        this.author = author;
        this.tags = tags;
    }

    /**
     * Instantiates a new serch criteria.
     */
    public SerchCriteria() {
    }

}
