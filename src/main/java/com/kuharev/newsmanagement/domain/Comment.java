package com.kuharev.newsmanagement.domain;

import java.sql.Timestamp;

/**
 * The Class Comment.
 */
public class Comment extends Entity {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * The text.
     */
    private String text;

    /**
     * The creation date.
     */
    private Timestamp creationDate;

    /**
     * Instantiates a new comment.
     */
    public Comment() {
    }

    /**
     * Instantiates a new comment.
     *
     * @param text         the text
     * @param creationDate the creation date
     */
    public Comment(String text, Timestamp creationDate) {
        super();
        this.text = text;
        this.creationDate = creationDate;
    }

    /**
     * Instantiates a new comment.
     *
     * @param id           the id
     * @param text         the text
     * @param creationDate the creation date
     */
    public Comment(Long id, String text, Timestamp creationDate) {
        super(id);
        this.text = text;
        this.creationDate = creationDate;
    }


    /**
     * Gets the text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text.
     *
     * @param text the new text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     *
     * @param creationDate the new creation date
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }


}
