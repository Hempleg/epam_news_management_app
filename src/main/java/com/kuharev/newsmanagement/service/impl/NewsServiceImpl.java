package com.kuharev.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.kuharev.newsmanagement.comparator.MostCommentedNewsComparator;
import com.kuharev.newsmanagement.dao.impl.NewsDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.AuthorService;
import com.kuharev.newsmanagement.service.CommentService;
import com.kuharev.newsmanagement.service.NewsService;
import com.kuharev.newsmanagement.service.TagService;

/**
 * The Class NewsServiceImpl.
 */
public class NewsServiceImpl implements NewsService {

    /**
     * The news dao.
     */
    private NewsDAO newsDAO;

    /**
     * The tag service.
     */
    private TagService tagService;

    /**
     * The author service.
     */
    private AuthorService authorService;

    /**
     * The comment service.
     */
    private CommentService commentService;


    /**
     * Sets the news dao.
     *
     * @param newsDAO the new news dao
     */
    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    /**
     * Sets the tag service.
     *
     * @param tagService the new tag service
     */
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Sets the author service.
     *
     * @param authorService the new author service
     */
    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    /**
     * Sets the comment service.
     *
     * @param commentService the new comment service
     */
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * Instantiates a new news service impl.
     */
    private NewsServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#addNews(com.kuharev.newsmanagement.domain.News)
     */
    @Override
    @Transactional
    public void addNews(News news) throws ServiceException {
        try {
            News nNews = newsDAO.create(news);
            news.setId(nNews.getId());

            addNewsTags(news.getId(), news.getTags());

            if (news.getAuthor() != null) {
                changeNewsAuthor(news.getId(), news.getAuthor().getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#editNews(com.kuharev.newsmanagement.domain.News)
     */
    @Override
    @Transactional
    public void editNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);

            authorService.changeNewsAuthor(news.getId(), news.getAuthor().getId());
            tagService.deleteTagsForNews(news.getId(), news.getTags());

            addNewsTags(news.getId(), news.getTags());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#deleteNews(java.lang.Long)
     */
    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#getAllNews()
     */
    @Override
    @Transactional
    public List<News> getAllNews() throws ServiceException {
        List<News> newsList = new ArrayList<News>();

        try {
            newsList.addAll(newsDAO.findAll());
            completeNewsList(newsList);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return newsList;
    }

    /**
     * Complete news.
     *
     * @param news the news
     * @throws ServiceException the service exception
     */
    private void completeNews(News news) throws ServiceException {
        Author author = authorService.findAuthorByNewsId(news.getId());
        List<Tag> tags = tagService.findTagsByNewsId(news.getId());
        List<Comment> comments = commentService.findCommentsByNewsId(news.getId());

        news.setAuthor(author);
        news.setTags(tags);
        news.setComments(comments);
    }

    /**
     * Complete news list.
     *
     * @param news the news
     * @throws ServiceException the service exception
     */
    private void completeNewsList(List<News> news) throws ServiceException {
        for (News n : news) {
            completeNews(n);
        }
    }


    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#findNews(java.lang.Long)
     */
    @Override
    @Transactional
    public News findNews(Long id) throws ServiceException {
        News news = null;

        try {
            news = newsDAO.find(id);
            completeNews(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return news;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#findNewsSortedByMostCommented()
     */
    @Override
    public List<News> findNewsSortedByMostCommented() throws ServiceException {
        List<News> newsList = getAllNews();

        Collections.sort(newsList, new MostCommentedNewsComparator());

        return newsList;
    }


    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#changeNewsAuthor(java.lang.Long, java.lang.Long)
     */
    @Override
    public void changeNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        authorService.changeNewsAuthor(newsId, authorId);
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#addNewsTag(java.lang.Long, com.kuharev.newsmanagement.domain.Tag)
     */
    @Override
    public void addNewsTag(Long newsId, Tag tag) throws ServiceException {
        tagService.addTagsForNews(newsId, Arrays.asList(tag));
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#addNewsTags(java.lang.Long, java.util.List)
     */
    @Override
    public void addNewsTags(Long newsId, List<Tag> tags) throws ServiceException {
        tagService.addTagsForNews(newsId, tags);
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#addCommentForNews(java.lang.Long, com.kuharev.newsmanagement.domain.Comment)
     */
    @Override
    public void addCommentForNews(Long newsId, Comment comment) throws ServiceException {
        commentService.addComment(newsId, comment);
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#countAllNews()
     */
    @Override
    public Integer countAllNews() throws ServiceException {
        return getAllNews().size();
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.NewsService#findNewsBySerchCriteria(com.kuharev.newsmanagement.domain.SerchCriteria)
     */
    @Override
    public List<News> findNewsBySerchCriteria(SerchCriteria sc) throws ServiceException {
        List<News> newsList = new ArrayList<News>();

        try {
            newsList.addAll(newsDAO.findBySerchCriteria(sc));
            completeNewsList(newsList);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return newsList;
    }

}
