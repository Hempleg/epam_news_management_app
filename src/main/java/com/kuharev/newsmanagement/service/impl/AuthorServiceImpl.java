package com.kuharev.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.kuharev.newsmanagement.dao.impl.AuthorDAO;
import com.kuharev.newsmanagement.dao.impl.NewsAuthorDAO;
import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.AuthorService;
import com.kuharev.newsmanagement.service.NewsService;

/**
 * The Class AuthorServiceImpl.
 */
public class AuthorServiceImpl implements AuthorService {

    /**
     * The author dao.
     */
    private AuthorDAO authorDAO;

    /**
     * The news author dao.
     */
    private NewsAuthorDAO newsAuthorDAO;

    /**
     * The news service.
     */
    private NewsService newsService;

    /**
     * The author service.
     */
    private AuthorService authorService;

    /**
     * Instantiates a new author service impl.
     */
    private AuthorServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#addAuthor(com.kuharev.newsmanagement.domain.Author)
     */
    @Override
    public void addAuthor(Author author) throws ServiceException {
        try {
            Author aAuthor = authorDAO.create(author);
            author.setId(aAuthor.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#deleteAuthor(java.lang.Long)
     */
    @Override
    public void deleteAuthor(Long authorId) throws ServiceException {
        try {
            authorDAO.delete(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#updateAuthor(com.kuharev.newsmanagement.domain.Author)
     */
    @Override
    public void updateAuthor(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#findAuthor(java.lang.Long)
     */
    @Override
    public Author findAuthor(Long id) throws ServiceException {
        Author author = null;

        try {
            author = authorDAO.find(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return author;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#getAllAuthors()
     */
    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authors = new ArrayList<Author>();

        try {
            authors.addAll(authorDAO.findAll());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return authors;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#findNewsByAuthor(java.lang.Long)
     */
    @Override
    @Transactional
    public List<News> findNewsByAuthor(Long authorId) throws ServiceException {
        List<News> news = new ArrayList<News>();
        try {
            List<Long> newsIds = newsAuthorDAO.readNewsIdsByAuthorId(authorId);

            for (Long id : newsIds) {
                news.add(newsService.findNews(id));
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return news;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#findAuthorByNewsId(java.lang.Long)
     */
    @Override
    @Transactional
    public Author findAuthorByNewsId(Long newsId) throws ServiceException {
        Author author = null;
        try {
            List<Long> authorIds = newsAuthorDAO.findAuthorIdsByNewsId(newsId);

            if (!authorIds.isEmpty()) {
                author = authorService.findAuthorByNewsId(newsId);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return author;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#changeNewsAuthor(java.lang.Long, java.lang.Long)
     */
    @Override
    @Transactional
    public void changeNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            Author authorToDelete = findAuthorByNewsId(newsId);

            if (authorToDelete != null) {
                newsAuthorDAO.delete(newsId, authorToDelete.getId());
            }

            newsAuthorDAO.create(newsId, authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.AuthorService#makeAuthorExpired(java.lang.Long, java.sql.Timestamp)
     */
    @Override
    public void makeAuthorExpired(Long authorId, Timestamp dateTime) throws ServiceException {
        Author author = findAuthor(authorId);
        author.setExpired(dateTime);

        updateAuthor(author);
    }

}
