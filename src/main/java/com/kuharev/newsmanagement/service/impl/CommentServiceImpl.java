package com.kuharev.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.kuharev.newsmanagement.dao.impl.CommentDAO;
import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.CommentService;

/**
 * The Class CommentServiceImpl.
 */
public class CommentServiceImpl implements CommentService {

    /**
     * The comment dao.
     */
    private CommentDAO commentDAO;


    /**
     * Sets the comment dao.
     *
     * @param commentDAO the new comment dao
     */
    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * Instantiates a new comment service impl.
     */
    private CommentServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.CommentService#addComment(java.lang.Long, com.kuharev.newsmanagement.domain.Comment)
     */
    @Override
    public void addComment(Long newsId, Comment comment) throws ServiceException {
        try {
            Comment cComment = commentDAO.create(newsId, comment);
            comment.setId(cComment.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.CommentService#deleteComment(java.lang.Long)
     */
    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.CommentService#updateComment(com.kuharev.newsmanagement.domain.Comment)
     */
    @Override
    public void updateComment(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.CommentService#findCommentsByNewsId(java.lang.Long)
     */
    @Override
    public List<Comment> findCommentsByNewsId(Long newsId) throws ServiceException {
        List<Comment> comments = new ArrayList<Comment>();
        try {
            comments.addAll(commentDAO.findByNewsId(newsId));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return comments;
    }

}
