package com.kuharev.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.kuharev.newsmanagement.dao.impl.NewsTagDAO;
import com.kuharev.newsmanagement.dao.impl.TagDAO;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.NewsService;
import com.kuharev.newsmanagement.service.TagService;

/**
 * The Class TagServiceImpl.
 */
public class TagServiceImpl implements TagService {

    /**
     * The tag dao.
     */
    private TagDAO tagDAO;

    /**
     * The news tag dao.
     */
    private NewsTagDAO newsTagDAO;

    /**
     * The news service.
     */
    private NewsService newsService;

    /**
     * Instantiates a new tag service impl.
     */
    private TagServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#addTag(com.kuharev.newsmanagement.domain.Tag)
     */
    @Override
    public void addTag(Tag tag) throws ServiceException {
        try {
            Tag tTag = tagDAO.create(tag);
            tag.setId(tTag.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#deleteTag(java.lang.Long)
     */
    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        try {
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#updateTag(com.kuharev.newsmanagement.domain.Tag)
     */
    @Override
    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#findTag(java.lang.Long)
     */
    @Override
    public Tag findTag(Long tagId) throws ServiceException {
        Tag tag = null;

        try {
            tag = tagDAO.find(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return tag;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#findTagsByNewsId(java.lang.Long)
     */
    @Override
    @Transactional
    public List<Tag> findTagsByNewsId(Long newsId) throws ServiceException {
        List<Tag> tags = new ArrayList<Tag>();
        try {
            List<Long> tagIds = newsTagDAO.findTagIdsByNewsId(newsId);

            for (Long id : tagIds) {
                tags.add(findTag(id));
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return tags;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#findNewsByTag(java.lang.Long)
     */
    @Override
    @Transactional
    public List<News> findNewsByTag(Long tagId) throws ServiceException {
        List<News> news = new ArrayList<News>();
        try {
            List<Long> tagIds = newsTagDAO.readNewsIdsByTagId(tagId);

            for (Long id : tagIds) {
                news.add(newsService.findNews(id));
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return news;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#addTagsForNews(java.lang.Long, java.util.List)
     */
    @Override
    @Transactional
    public void addTagsForNews(Long newsId, List<Tag> tags) throws ServiceException {
        try {
            for (Tag t : tags) {
                newsTagDAO.create(newsId, t.getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#deleteTagsForNews(java.lang.Long, java.util.List)
     */
    @Override
    @Transactional
    public void deleteTagsForNews(Long newsId, List<Tag> tags) throws ServiceException {
        try {
            for (Tag t : tags) {
                newsTagDAO.delete(newsId, t.getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.TagService#getAllTags()
     */
    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags = new ArrayList<>();
        try {
            tags.addAll(tagDAO.readAll());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return tags;
    }

}
