package com.kuharev.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.kuharev.newsmanagement.dao.impl.RoleDAO;
import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.RoleService;
import com.kuharev.newsmanagement.service.UserService;
import com.kuharev.newsmanagement.dao.impl.UserRoleDAO;

/**
 * The Class RoleServiceImpl.
 */
public class RoleServiceImpl implements RoleService {

    /**
     * The role dao.
     */
    private RoleDAO roleDAO;

    /**
     * The user role dao.
     */
    private UserRoleDAO userRoleDAO;

    /**
     * The user service.
     */
    private UserService userService;

    /**
     * Gets the role dao.
     *
     * @return the role dao
     */
    public RoleDAO getRoleDAO() {
        return roleDAO;
    }

    /**
     * Sets the role dao.
     *
     * @param roleDAO the new role dao
     */
    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }

    /**
     * Gets the user role dao.
     *
     * @return the user role dao
     */
    public UserRoleDAO getUserRoleDAO() {
        return userRoleDAO;
    }

    /**
     * Sets the user role dao.
     *
     * @param userRoleDAO the new user role dao
     */
    public void setUserRoleDAO(UserRoleDAO userRoleDAO) {
        this.userRoleDAO = userRoleDAO;
    }

    /**
     * Instantiates a new role service impl.
     */
    private RoleServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#addRole(com.kuharev.newsmanagement.domain.Role)
     */
    @Override
    public void addRole(Role role) throws ServiceException {
        try {
            Role rRole = roleDAO.create(role);
            role.setId(rRole.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#deleteRole(java.lang.Long)
     */
    @Override
    public void deleteRole(Long roleId) throws ServiceException {
        try {
            roleDAO.delete(roleId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#updateRole(com.kuharev.newsmanagement.domain.Role)
     */
    @Override
    public void updateRole(Role role) throws ServiceException {
        try {
            roleDAO.update(role);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#findRole(java.lang.Long)
     */
    @Override
    public Role findRole(Long id) throws ServiceException {
        Role role = null;
        try {
            role = roleDAO.find(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return role;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#findUsersByRole(java.lang.Long)
     */
    @Override
    @Transactional
    public List<User> findUsersByRole(Long roleId) throws ServiceException {
        List<User> users = new ArrayList<User>();
        try {
            List<Long> userIds = userRoleDAO.readUserIdsByRoleId(roleId);

            for (Long id : userIds) {
                users.add(userService.findUser(id));
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return users;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#findRoleByUserId(java.lang.Long)
     */
    @Override
    @Transactional
    public Role findRoleByUserId(Long userId) throws ServiceException {
        Role role = null;
        try {
            Long id = userRoleDAO.readRoleIdByUserId(userId);

            role = findRole(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return role;
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#changeUserRole(java.lang.Long, java.lang.Long)
     */
    @Override
    @Transactional
    public void changeUserRole(Long userId, Long roleId) throws ServiceException {
        try {
            Role role = findRoleByUserId(userId);

            if (role != null) {
                deleteRoleFromUser(userId, role.getId());
            }
            userRoleDAO.create(userId, roleId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#deleteRoleFromUser(java.lang.Long, java.lang.Long)
     */
    @Override
    @Transactional
    public void deleteRoleFromUser(Long userId, Long roleId) throws ServiceException {
        try {
            userRoleDAO.delete(userId, roleId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.RoleService#getAllRoles()
     */
    @Override
    public List<Role> getAllRoles() throws ServiceException {
        List<Role> roles = new ArrayList<>();
        try {
            roles = roleDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return roles;
    }

}
