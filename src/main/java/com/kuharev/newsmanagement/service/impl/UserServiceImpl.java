package com.kuharev.newsmanagement.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.kuharev.newsmanagement.dao.impl.UserDAO;
import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.DAOException;
import com.kuharev.newsmanagement.exeption.ServiceException;
import com.kuharev.newsmanagement.service.RoleService;
import com.kuharev.newsmanagement.service.UserService;

/**
 * The Class UserServiceImpl.
 */
public class UserServiceImpl implements UserService {

    /**
     * The user dao.
     */
    private UserDAO userDAO;

    /**
     * The role service.
     */
    private RoleService roleService;


    /**
     * Gets the user dao.
     *
     * @return the user dao
     */
    public UserDAO getUserDAO() {
        return userDAO;
    }

    /**
     * Sets the user dao.
     *
     * @param userDAO the new user dao
     */
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Gets the role service.
     *
     * @return the role service
     */
    public RoleService getRoleService() {
        return roleService;
    }

    /**
     * Sets the role service.
     *
     * @param roleService the new role service
     */
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    /**
     * Instantiates a new user service impl.
     */
    private UserServiceImpl() {
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.UserService#addUser(com.kuharev.newsmanagement.domain.User)
     */
    @Override
    @Transactional
    public void addUser(User user) throws ServiceException {
        try {
            User uUser = userDAO.create(user);
            user.setId(uUser.getId());

            if (user.getRole() != null) {
                roleService.changeUserRole(user.getId(), user.getRole().getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.UserService#deleteUser(java.lang.Long)
     */
    @Override
    public void deleteUser(Long userId) throws ServiceException {
        try {
            userDAO.delete(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.UserService#updateUser(com.kuharev.newsmanagement.domain.User)
     */
    @Override
    @Transactional
    public void updateUser(User user) throws ServiceException {
        try {
            userDAO.update(user);

            Role role = roleService.findRoleByUserId(user.getId());
            roleService.changeUserRole(user.getId(), user.getRole().getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.UserService#findUser(java.lang.Long)
     */
    @Override
    @Transactional
    public User findUser(Long id) throws ServiceException {
        User user = null;

        try {
            user = userDAO.find(id);
            completeUser(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return user;
    }

    /**
     * Complete user.
     *
     * @param user the user
     * @throws ServiceException the service exception
     */
    private void completeUser(User user) throws ServiceException {
        Role role = roleService.findRoleByUserId(user.getId());

        user.setRole(role);
    }

    /* (non-Javadoc)
     * @see com.kuharev.newsmanagement.service.UserService#getAllUsers()
     */
    @Override
    public List<User> getAllUsers() throws ServiceException {
        List<User> users = new ArrayList<>();

        try {
            users.addAll(userDAO.readAll());

            completeUsers(users);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return users;
    }

    /**
     * Complete users.
     *
     * @param users the users
     * @throws ServiceException the service exception
     */
    private void completeUsers(List<User> users) throws ServiceException {
        for (User user : users) {
            completeUser(user);
        }

    }


}
