package com.kuharev.newsmanagement.service;

import java.sql.Timestamp;
import java.util.List;

import com.kuharev.newsmanagement.domain.Author;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface AuthorService.
 */
public interface AuthorService {

    /**
     * Adds the author.
     *
     * @param author the author
     * @throws ServiceException the service exception
     */
    void addAuthor(Author author) throws ServiceException;

    /**
     * Delete author.
     *
     * @param authorId the author id
     * @throws ServiceException the service exception
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Update author.
     *
     * @param author the author
     * @throws ServiceException the service exception
     */
    void updateAuthor(Author author) throws ServiceException;

    /**
     * Find author.
     *
     * @param id the id
     * @return the author
     * @throws ServiceException the service exception
     */
    Author findAuthor(Long id) throws ServiceException;

    /**
     * Gets the all authors.
     *
     * @return the all authors
     * @throws ServiceException the service exception
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Find news by author.
     *
     * @param authorId the author id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<News> findNewsByAuthor(Long authorId) throws ServiceException;

    /**
     * Find author by news id.
     *
     * @param newsId the news id
     * @return the author
     * @throws ServiceException the service exception
     */
    Author findAuthorByNewsId(Long newsId) throws ServiceException;

    /**
     * Change news author.
     *
     * @param newsId   the news id
     * @param authorId the author id
     * @throws ServiceException the service exception
     */
    void changeNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Make author expired.
     *
     * @param authorId the author id
     * @param dateTime the date time
     * @throws ServiceException the service exception
     */
    void makeAuthorExpired(Long authorId, Timestamp dateTime) throws ServiceException;
}
