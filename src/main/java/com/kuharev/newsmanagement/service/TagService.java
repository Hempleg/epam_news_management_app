package com.kuharev.newsmanagement.service;

import java.util.List;

import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface TagService.
 */
public interface TagService {

    /**
     * Adds the tag.
     *
     * @param tag the tag
     * @throws ServiceException the service exception
     */
    void addTag(Tag tag) throws ServiceException;

    /**
     * Delete tag.
     *
     * @param tagId the tag id
     * @throws ServiceException the service exception
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Update tag.
     *
     * @param tag the tag
     * @throws ServiceException the service exception
     */
    void updateTag(Tag tag) throws ServiceException;

    /**
     * Gets the all tags.
     *
     * @return the all tags
     * @throws ServiceException the service exception
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Find tag.
     *
     * @param tagId the tag id
     * @return the tag
     * @throws ServiceException the service exception
     */
    Tag findTag(Long tagId) throws ServiceException;

    /**
     * Find tags by news id.
     *
     * @param newsId the news id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Tag> findTagsByNewsId(Long newsId) throws ServiceException;

    /**
     * Find news by tag.
     *
     * @param tagId the tag id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<News> findNewsByTag(Long tagId) throws ServiceException;

    /**
     * Adds the tags for news.
     *
     * @param newsId the news id
     * @param tags   the tags
     * @throws ServiceException the service exception
     */
    void addTagsForNews(Long newsId, List<Tag> tags) throws ServiceException;

    /**
     * Delete tags for news.
     *
     * @param newsId the news id
     * @param tags   the tags
     * @throws ServiceException the service exception
     */
    void deleteTagsForNews(Long newsId, List<Tag> tags) throws ServiceException;
}
