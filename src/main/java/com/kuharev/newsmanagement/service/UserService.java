package com.kuharev.newsmanagement.service;

import java.util.List;

import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface UserService.
 */
public interface UserService {

    /**
     * Adds the user.
     *
     * @param user the user
     * @throws ServiceException the service exception
     */
    void addUser(User user) throws ServiceException;

    /**
     * Delete user.
     *
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    void deleteUser(Long userId) throws ServiceException;

    /**
     * Update user.
     *
     * @param user the user
     * @throws ServiceException the service exception
     */
    void updateUser(User user) throws ServiceException;

    /**
     * Find user.
     *
     * @param id the id
     * @return the user
     * @throws ServiceException the service exception
     */
    User findUser(Long id) throws ServiceException;

    /**
     * Gets the all users.
     *
     * @return the all users
     * @throws ServiceException the service exception
     */
    List<User> getAllUsers() throws ServiceException;
}
