package com.kuharev.newsmanagement.service;

import java.util.List;

import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface CommentService.
 */
public interface CommentService {

    /**
     * Adds the comment.
     *
     * @param newsId  the news id
     * @param comment the comment
     * @throws ServiceException the service exception
     */
    void addComment(Long newsId, Comment comment) throws ServiceException;

    /**
     * Delete comment.
     *
     * @param commentId the comment id
     * @throws ServiceException the service exception
     */
    void deleteComment(Long commentId) throws ServiceException;

    /**
     * Update comment.
     *
     * @param comment the comment
     * @throws ServiceException the service exception
     */
    void updateComment(Comment comment) throws ServiceException;

    /**
     * Find comments by news id.
     *
     * @param newsId the news id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Comment> findCommentsByNewsId(Long newsId) throws ServiceException;
}
