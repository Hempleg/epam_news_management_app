package com.kuharev.newsmanagement.service;

import java.util.List;

import com.kuharev.newsmanagement.domain.Comment;
import com.kuharev.newsmanagement.domain.News;
import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.domain.Tag;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface NewsService.
 */
public interface NewsService {

    /**
     * Adds the news.
     *
     * @param news the news
     * @throws ServiceException the service exception
     */
    void addNews(News news) throws ServiceException;

    /**
     * Edits the news.
     *
     * @param news the news
     * @throws ServiceException the service exception
     */
    void editNews(News news) throws ServiceException;

    /**
     * Delete news.
     *
     * @param newsId the news id
     * @throws ServiceException the service exception
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * Gets the all news.
     *
     * @return the all news
     * @throws ServiceException the service exception
     */
    List<News> getAllNews() throws ServiceException;

    /**
     * Find news.
     *
     * @param id the id
     * @return the news
     * @throws ServiceException the service exception
     */
    News findNews(Long id) throws ServiceException;

    /**
     * Find news sorted by most commented.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<News> findNewsSortedByMostCommented() throws ServiceException;

    /**
     * Change news author.
     *
     * @param newsId   the news id
     * @param authorId the author id
     * @throws ServiceException the service exception
     */
    void changeNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Adds the news tag.
     *
     * @param newsId the news id
     * @param tag    the tag
     * @throws ServiceException the service exception
     */
    void addNewsTag(Long newsId, Tag tag) throws ServiceException;

    /**
     * Adds the news tags.
     *
     * @param newsId the news id
     * @param tags   the tags
     * @throws ServiceException the service exception
     */
    void addNewsTags(Long newsId, List<Tag> tags) throws ServiceException;

    /**
     * Adds the comment for news.
     *
     * @param newsId  the news id
     * @param comment the comment
     * @throws ServiceException the service exception
     */
    void addCommentForNews(Long newsId, Comment comment) throws ServiceException;

    /**
     * Count all news.
     *
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer countAllNews() throws ServiceException;

    /**
     * Find news by serch criteria.
     *
     * @param sc the sc
     * @return the list
     * @throws ServiceException the service exception
     */
    List<News> findNewsBySerchCriteria(SerchCriteria sc) throws ServiceException;
}