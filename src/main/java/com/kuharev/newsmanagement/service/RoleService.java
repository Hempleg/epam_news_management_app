package com.kuharev.newsmanagement.service;

import java.util.List;

import com.kuharev.newsmanagement.domain.Role;
import com.kuharev.newsmanagement.domain.User;
import com.kuharev.newsmanagement.exeption.ServiceException;

/**
 * The Interface RoleService.
 */
public interface RoleService {

    /**
     * Adds the role.
     *
     * @param role the role
     * @throws ServiceException the service exception
     */
    void addRole(Role role) throws ServiceException;

    /**
     * Delete role.
     *
     * @param roleId the role id
     * @throws ServiceException the service exception
     */
    void deleteRole(Long roleId) throws ServiceException;

    /**
     * Update role.
     *
     * @param role the role
     * @throws ServiceException the service exception
     */
    void updateRole(Role role) throws ServiceException;

    /**
     * Gets the all roles.
     *
     * @return the all roles
     * @throws ServiceException the service exception
     */
    List<Role> getAllRoles() throws ServiceException;

    /**
     * Find role.
     *
     * @param id the id
     * @return the role
     * @throws ServiceException the service exception
     */
    Role findRole(Long id) throws ServiceException;

    /**
     * Find users by role.
     *
     * @param roleId the role id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<User> findUsersByRole(Long roleId) throws ServiceException;

    /**
     * Find role by user id.
     *
     * @param userId the user id
     * @return the role
     * @throws ServiceException the service exception
     */
    Role findRoleByUserId(Long userId) throws ServiceException;

    /**
     * Change user role.
     *
     * @param userId the user id
     * @param roleId the role id
     * @throws ServiceException the service exception
     */
    void changeUserRole(Long userId, Long roleId) throws ServiceException;

    /**
     * Delete role from user.
     *
     * @param userId the user id
     * @param roleId the role id
     * @throws ServiceException the service exception
     */
    public void deleteRoleFromUser(Long userId, Long roleId) throws ServiceException;
}
