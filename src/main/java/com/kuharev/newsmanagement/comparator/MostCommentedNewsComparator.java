package com.kuharev.newsmanagement.comparator;

import java.util.Comparator;

import com.kuharev.newsmanagement.domain.News;

/**
 * The Class with comporator (compare news by most commented, order -  desc).
 */
public class MostCommentedNewsComparator implements Comparator<News> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(News news1, News news2) {
        return news1.getComments().size() - news2.getComments().size();
    }

}
