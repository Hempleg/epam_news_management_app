package com.kuharev.newsmanagement.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.dbunit.database.statement.PreparedBatchStatement;

import com.kuharev.newsmanagement.domain.SerchCriteria;
import com.kuharev.newsmanagement.domain.Tag;

/**
 * The Class SerchUtil.
 */
public class SerchUtil {

    /**
     * The Constant SQL_FIND_BY_AUTHOR_AND_TAGS.
     */
    public static final String SQL_FIND_BY_AUTHOR_AND_TAGS = "SELECT news_id, title, short_text,"
            + " full_text, creation_date, modification_date "
            + "FROM news JOIN news_authors USING (news_id) "
            + "JOIN news_tags USING (news_Id) WHERE "
            + "author_id=? AND tag_id IN(";

    /**
     * The Constant SQL_FIND_BY_AUTHOR.
     */
    public static final String SQL_FIND_BY_AUTHOR = "SELECT news_id, title, short_text, full_text, "
            + "creation_date, modification_date FROM news JOIN "
            + "news_authors USING (news_id) WHERE author_id=?";

    /**
     * The Constant SQL_FIND_BY_TAGS.
     */
    public static final String SQL_FIND_BY_TAGS = "SELECT news_id, title, short_text,"
            + " full_text, creation_date, "
            + "modification_date FROM news JOIN "
            + "news_tags USING (news_Id) "
            + "WHERE tag_id IN(";

    /**
     * Builds the query.
     *
     * @param sc the serch criteria
     * @return the string
     */
    public static String buildQuery(SerchCriteria sc) {
        StringBuilder builder = new StringBuilder();

        if (sc.getAuthor() != null && sc.getTags() != null && !sc.getTags().isEmpty()) {
            for (int i = 0; i < sc.getTags().size(); i++) {
                builder.append("?,");
            }

            return SQL_FIND_BY_AUTHOR_AND_TAGS + builder.deleteCharAt(builder.length() - 1).toString() + ")";
        } else if (sc.getAuthor() == null && sc.getTags() != null && !sc.getTags().isEmpty()) {
            for (int i = 0; i < sc.getTags().size(); i++) {
                builder.append("?,");
            }

            return SQL_FIND_BY_TAGS + builder.deleteCharAt(builder.length() - 1).toString() + ")";
        } else {
            return SQL_FIND_BY_AUTHOR;
        }
    }

    /**
     * Fill prepared statement.
     *
     * @param sc the serch criteria
     * @param ps the prepared statement
     * @throws SQLException the SQL exception
     */
    public static void fillPreparedStatement(SerchCriteria sc, PreparedStatement ps) throws SQLException {
        int index = 1;
        if (sc.getAuthor() != null) {
            ps.setLong(index++, sc.getAuthor().getId());
        }

        if (sc.getTags() != null && !sc.getTags().isEmpty()) {
            for (Tag tag : sc.getTags()) {
                ps.setLong(index++, tag.getId());
            }
        }

    }

}
