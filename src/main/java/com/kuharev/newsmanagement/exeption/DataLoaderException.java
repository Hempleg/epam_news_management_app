package com.kuharev.newsmanagement.exeption;

/**
 * The Class DataLoaderException.
 */
public class DataLoaderException extends Exception {

    /**
     * Instantiates a new data loader exception.
     */
    public DataLoaderException() {
        super();
    }

    /**
     * Instantiates a new data loader exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     * @param arg2 the arg2
     * @param arg3 the arg3
     */
    public DataLoaderException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    /**
     * Instantiates a new data loader exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     */
    public DataLoaderException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new data loader exception.
     *
     * @param arg0 the arg0
     */
    public DataLoaderException(String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new data loader exception.
     *
     * @param arg0 the arg0
     */
    public DataLoaderException(Throwable arg0) {
        super(arg0);
    }

}
