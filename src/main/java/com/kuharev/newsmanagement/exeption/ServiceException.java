package com.kuharev.newsmanagement.exeption;

/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {

    /**
     * Instantiates a new service exception.
     */
    public ServiceException() {
        super();
    }

    /**
     * Instantiates a new service exception.
     *
     * @param arg0 the arg0
     */
    public ServiceException(String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new service exception.
     *
     * @param arg0 the arg0
     */
    public ServiceException(Throwable arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new service exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     */
    public ServiceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new service exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     * @param arg2 the arg2
     * @param arg3 the arg3
     */
    public ServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

}
